'''
Created on Feb 23, 2023

@author: Lobnury
'''
from include import *
#
#---------------------------------------------------------------
#
nbP = 4 # number of parallel executions
timeout = "91800s" # or "86400s"
#
#=====================================================================================
#=====================================================================================
#
def create_folder(folder):
    os.makedirs(folder,exist_ok=True)
#
#---------------------------------------------------------------
#
def run_instance(queue, jar_file, config):
    try:
        (dataname, datafile, pat, feat, ag, window, deltaX, wPercent, ov) = config
        #dataname = os.path.splitext(os.path.basename(datafile))[0]
        #
        if not os.path.exists(jar_file):
            print(f"\nIMPOSSIBLE TO LAUNCH EXECUTION, BUILD FIRST \n")
            sys.exit(1)
        #
        res_dir = os.path.realpath(os.path.join(results_dir, "XP-10-05-2023", pat, dataname))
        res_dir = os.path.realpath(os.path.join(res_dir, "window-"+str(window), "deltaX-"+str(deltaX)))
        create_folder(res_dir)
        #
        filename = dataname + "-wp_" + str(wPercent) + "-p_" + str(ov)
        time_file = os.path.join(res_dir, filename + ".time")
        logs_file = os.path.join(res_dir, filename + ".log")
        #
        prg_dependencies = ""
        prg_dependencies += "TimeSeries-Code/experiments/build/libs/experiments-0.0.1-SNAPSHOT.jar"
        prg_dependencies += ":TimeSeries-Code/lib/build/libs/lib-0.0.1-SNAPSHOT.jar"
        prg_dependencies += ":libs/commons-cli-1.5.0.jar"
        prg_dependencies += ":libs/commons-lang3-3.12.0.jar"
        prg_dependencies += ":libs/opencsv-5.7.1.jar"
        #
        #command = f"timeout {timeLimit} time -v -o {time_file}"
        command = f"time -v -o {time_file} java -jar {jar_file}"
        command = f"time -v -o {time_file} java -classpath {prg_dependencies} fr.imtAtlantique.ls2n.tasc.timeSeries.xps.Main"
        command += f" -p {pat} -f {feat} -a {ag} -w {window} -dx {deltaX} -wp {wPercent} -ov {ov} -d {data_file}"
        #
        m_logs = open(logs_file, "w")
        subprocess.run([command + "; echo \" \n\n\nExit status: $?\" "], shell=True, check=True, stdout=m_logs, stderr=m_logs)
        #
    finally:
        queue.put(mp.current_process().name)
#
#=====================================================================================
#=====================================================================================
#
if __name__ == '__main__':
    #
    # prepare the different configurations
    configs = []
    for dataname in all_dataname:
        dataset = dataname+".csv"
        data_file = os.path.join(data_dir, dataset)
        for pattern in all_patterns:
            for deltaX in all_deltaX[dataname]:
                for window in windowsSizes:
                    for wp in all_epsilon:
                        for p in lambda_val:
                            configs.append((dataname, data_file, pattern, feature, aggreg, window, deltaX, wp, p))
    #
    jar_file = os.path.join(root_dir, "TimeSeries-Code", "experiments", "build", "libs", "experiments-0.0.1-SNAPSHOT.jar")
    #
    # launch processes one after the other, nbP processes can be launched in parallel
    curr = 0
    procs = dict()
    queue = mp.Queue() # queue of ongoing process
    for i in range(0, nbP):
        if curr < len(configs):
            pName = "dataset: " + str(configs[curr][0]) + " * " + str(configs[curr][2]) + " * w=" + str(configs[curr][5])
            pName += " * dx=" + str(configs[curr][6]) + " * wp=" + str(configs[curr][7]) + " * p=" + str(configs[curr][8])
            proc = Process(name=pName, target=run_instance, args=(queue, jar_file, configs[curr], ))
            proc.start()
            procs[proc.name] = proc
            curr += 1
    # using the queue, launch a new process whenever an old process finishes its workload
    while procs:
        name = queue.get()
        proc = procs[name]
        print(proc) 
        proc.join()
        del procs[name]
        if curr < len(configs):
            pName = "dataset: " + str(configs[curr][0]) + " * " + str(configs[curr][2]) + " * w=" + str(configs[curr][5])
            pName += " * dx=" + str(configs[curr][6]) + " * wp=" + str(configs[curr][7]) + " * p=" + str(configs[curr][8])
            proc = Process(name=pName, target=run_instance, args=(queue, jar_file, configs[curr], ))
            proc.start()
            procs[proc.name] = proc
            curr += 1
    #
    print("\n***** FINISHED *****\n")
    
    
    
    
    #########################################################################################################################################################

