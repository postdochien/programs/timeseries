# This Python 3 environment comes with many helpful analytics libraries installed
#
# @Author : Lobnury
#
#
from include import *
#
nbP = 6 # number of parallel executions
#
#
# --------------------------------------------------------#
# ---------------------- FUNCTIONS -----------------------#
# --------------------------------------------------------#
#
from include import *
#
def create_folder(folder):
    os.makedirs(folder,exist_ok=True)
    #
#
#=====================================================================================
#=====================================================================================
#
def save_to_csv(data, csv_filename, header=['floatvalue']):
    path_to_csv = os.path.abspath(os.path.join(data_dir, 'new', csv_filename))
    #
    with open(path_to_csv, 'w', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(header)
        writer.writerows(data)
    #
#
#=====================================================================================
#=====================================================================================
#
def load_csv_data(dataname):
    path_to_csv = os.path.abspath(os.path.join(data_dir, dataname))
    data_df = pd.read_csv(path_to_csv)
    return data_df
    #
#
#=====================================================================================
#=====================================================================================
#
def read_anomalies_data(config):
    (dataname, mPattern, window, deltaX, wPercent, ov) = config
    path_to_csv = os.path.abspath(os.path.join(results_dir, 'anomalies-data', 'XP-10-05-2023', mPattern, dataname))
    path_to_csv = os.path.abspath(os.path.join(path_to_csv, 'window-'+str(window), 'deltaX-'+str(deltaX)))
    csv_filename = dataname + "-wp_" + str(wPercent) + "-p_" + str(ov) + ".csv"
    data_csv = os.path.abspath(os.path.join(path_to_csv, csv_filename))
    #
    header=['inf', 'sup']
    data = pd.read_csv(data_csv)
    #
    data_keys = data.keys()
    data_inf = list(data['inf'])
    data_sup = list(data['sup'])
    #
    anomalies_data = list(np.array([data_inf, data_sup]).transpose())
    #
    return anomalies_data
    #
#
#=====================================================================================
#=====================================================================================
#
def parse_date_time(datatime_list):
    result = []
    for elem in datatime_list:
        elem_list = elem.split(" ")
        date_elem = list(map(int, elem_list[0].split("-")))
        time_elem = list(map(int, elem_list[1].split(":")))
        #
        all_datetime_elem = date_elem + time_elem
        datetime_object = datetime(*all_datetime_elem)
        result.append(datetime_object)
        #
    return result
    #
#
#=====================================================================================
#=====================================================================================
#
def merge_anomalies_data_NEW(anomalies_data, nbVal, window):
    merge_data = []
    (x1, x2) = (-1, -1)
    (inf, sup) = (-1, -1)
    #print(f"\n\n *** {len(anomalies_data)} - {nbVal-window+1} - {anomalies_data[:1]}*** ")
    for i in range(nbVal-window+1):
        if len(anomalies_data) == 0:
            break
        [x1, x2] = list(anomalies_data[0])
        #
        if ((i+1) == x1):
            if inf == -1:
                inf = x1
            sup = x2
            anomalies_data = anomalies_data[1:]
        elif inf != -1:
            merge_data.append([inf, sup])
            (inf, sup) = (-1, -1)
        #
    print(f"\n{merge_data[:3]}")
    return merge_data
    #
#=====================================================================================
def merge_anomalies_data(anomalies_data, nbVal, window, begin=0, end=0):
    merge_data = []
    (x1, x2) = (-1, -1)
    (inf, sup) = (-1, -1)
    #print(f"\n\n *** {len(anomalies_data)} - {nbVal-window+1} - {anomalies_data[:1]}*** ")
    #for i in range(nbVal-window+1):
    #
    '''
    i = 1
    #while i <= nbVal-window+1:
        if len(anomalies_data) == 0:
            break
        [x1, x2] = list(anomalies_data[0])
        #
        if (i == x1) or (x1 <= sup):
            if inf == -1:
                inf = x1
            sup = x2
            anomalies_data = anomalies_data[1:]
            i = i+1
        elif inf != -1:
            merge_data.append([inf, sup])
            (inf, sup) = (-1, -1)
            i = x1
        else:
            i = i+1
        #
    '''
    i = 0
    while i < len(anomalies_data):
        [x1, x2] = list(anomalies_data[i])
        if (x1 < begin):
            i = i+1
            continue
        if (x2 > end):
            if inf != -1:
                merge_data.append([inf, sup])
                (inf, sup) = (-1, -1)
            break
        #
        if (inf == -1): # or (i==0):
            inf = x1
            sup = x2
        elif x1 <= sup:
            sup = x2
        else:
            merge_data.append([inf, sup])
            (inf, sup) = (-1, -1)
        #
        i = i+1
    #
    #print(f"\n{merge_data[:3]}")
    return merge_data
#
#=====================================================================================
#=====================================================================================
#
#def draw_sliding_window_graph():
def draw_graph(queue, config):
    try:
        queue.put(mp.current_process().name)
        #
        (dataname, dataset, mPattern, window, deltaX, wPercent, ov) = config
        #
        mTitle = dataname + rf" - $\sigma$={mPattern} - $m$={window} - $\delta_X$={deltaX} - $\epsilon$={wPercent}"
        mTitle = f"DataSet: {dataname}" + rf" - Params: $m$={window}, $\delta_X$={deltaX}, $\epsilon$={wPercent}"
        #
        df = load_csv_data(dataset)
        df = df[(df['Temperature'] != "error") | (df['Temperature'] != None) | (df['Humidity'] != None)]
        #
        data_keys = df.keys()
        #data_values = list(df['floatvalue'])
        data_values = list( map(float, list(df['Temperature'])) )
        data_values_hum = list( map(float, list(df['Humidity'])) )
        #time_values = parse_date_time( list(df['timestamp']) )
        #
        (low, up, nbG) = (200850, 200970, 1)
        #(low, up, nbG) = (800, 1400, 1)
        (low, up, nbG) = (0000, len(data_values), 1)
        mStep = int((up-low)/nbG)
        p = 1
        #
        iter_val = [int(((i+1)*(up-low))/nbG)  for i in range(nbG)]
        e = low
        for i in range(len(iter_val)):
            elem = iter_val[i] + low
            nbVal = elem - e
            #
            #+++++++++++++++++++++++++++++++++++++++++++++++++++++++
            #
            (low_, up_, mStep_) = (e, elem, int(mStep/10))
            all_x_ticks = [i for i in range(low_+1, up_+1, mStep_)]
            all_x_ticks_str = list(map(str, all_x_ticks))
            #
            #+++++++++++++++++++++++++++++++++++++++++++++++++++++++
            #
            y = data_values[e:elem]
            y2 = data_values_hum[e:elem]
            x = [e+k+1 for k in range(nbVal)]
            s = ["<" if y[k]<y[k+1] else (">" if y[k]>y[k+1] else "=") for k in range(nbVal-1) ]
            #
            #-------------------------------------
            #
            config_ = (dataname, mPattern, window, deltaX, wPercent, ov)
            anomalies_data = read_anomalies_data(config_)
            anomalies_data = merge_anomalies_data(anomalies_data, nbVal, window, low_, up_)
            #print(f"\n\n ===> {len(anomalies_data)} \n\n")
            #
            #+++++++++++++++++++++++++++++++++++++++++++++++++++++++
            #
            x_str = list(map(str, x))
            y_str = list(map(str, list(set(y)) ) )
            #
            '''
            #
            plt.figure(figsize=(12.0, 8.0))
            plt.plot(x, y, color=all_colors[0], linewidth=1, label="Temperature")
            for j in range(len(anomalies_data)):
                [inf, sup] = anomalies_data[j]
                if j==0:
                    plt.plot(x[inf-1:sup-1], y[inf-1:sup-1], color=all_colors[1], linewidth=1, label="Anomalies")
                else:
                    plt.plot(x[inf-1:sup-1], y[inf-1:sup-1], color=all_colors[1], linewidth=1)
            #
            '''
            #
            #'''
            # ---------------------------------------------------------------------------------------------------------------- #
            # ---------------------------------------------------------------------------------------------------------------- #
            #
            fig, (ax1, ax2) = plt.subplots(2, sharex="col", sharey="row", figsize=(12.0, 8.0))
            #
            titre = dataname + " - " + r"Time series $\mathcal{X} = $"rf"$ \langle X_{{ {e+1} }}, X_{{ {e+2} }}, \dots, X_{{ {elem} }} \rangle$"
            titre = mTitle
            #ax1.set_title(titre, fontsize=16)
            #---------------------------------------------------------------------
            ax1.plot(x, y, color=all_colors[0], linewidth=1, label="Temperature")
            #print(f"{x[:3]} - {anomalies_data[:3]} - ({low_}, {up_})")
            #
            ax1.set_ylabel(r"Temperature $\mathcal{X}^{1}$", fontsize=16)
            #---------------------------------------------------------------------
            ax2.plot(x, y2, color=all_colors[0], linewidth=1, label="Humidity")
            ax2.set_ylabel(r"Humidity $\mathcal{X}^{2}$", fontsize=16)
            #
            for j in range(len(anomalies_data)):
                [inf, sup] = anomalies_data[j]
                #
                #'''
                xi = [elt for elt in x if (elt>=inf and elt<=sup)]
                id_x = [ x.index(elt) for elt in xi ]
                yi1 = [y[t] for t in id_x]
                yi2 = [y2[t] for t in id_x]
                #'''
                #
                if j==0:
                    #print(f"({inf}, {sup}) - ({low_}, {up_}) - {xi}")
                    #ax1.plot(x[inf:sup], y[inf:sup], color=all_colors[1], linewidth=1, label="Anomalies")
                    ax1.plot(xi, yi1, color=all_colors[1], linewidth=1, label="Anomalies")
                    ax2.plot(xi, yi2, color=all_colors[1], linewidth=1, label="Anomalies")
                else:
                    #ax1.plot(x[inf:sup], y[inf:sup], color=all_colors[1], linewidth=1)
                    ax1.plot(xi, yi1, color=all_colors[1], linewidth=1)
                    ax2.plot(xi, yi2, color=all_colors[1], linewidth=1)
            #
            #ax1.legend(loc="best", fontsize=18)
            #ax2.legend(loc="best", fontsize=18)
            #
            # ---------------------------------------------------------------------------------------------------------------- #
            # ---------------------------------------------------------------------------------------------------------------- #
            #'''
            #
            #+++++++++++++++++++++++++++++++++++++++++++++++++++++++
            #
            plt.xticks(all_x_ticks, all_x_ticks_str, rotation=45)
            #plt.yticks(list(set(y)), y_str)
            #
            plt.xlabel("Time", fontsize=16)
            #plt.ylabel("Temperature", fontsize=16)
            #
            #plt.title(mTitle, fontsize=16)
            #
            #plt.legend(loc="best", fontsize=18)
            #plt.grid()
            plt.tight_layout()
            #
            plt.subplots_adjust(hspace=0, wspace=0)
            #
            #+++++++++++++++++++++++++++++++++++++++++++++++++++++++
            #
            mFolder = os.path.abspath(os.path.join(graphs_dir, "All-Datasets-with-anomalies"))
            png_folder = os.path.abspath(os.path.join(mFolder, "png", mPattern, dataname, "window-"+str(window), "deltaX-"+str(deltaX)))
            eps_folder = os.path.abspath(os.path.join(mFolder, "eps", mPattern, dataname, "window-"+str(window), "deltaX-"+str(deltaX)))
            create_folder(png_folder)
            create_folder(eps_folder)
            #
            filename = dataname + f"-wp_{wPercent}-p_{ov}-{i+1}"
            filename = f"anomaly-p_{mPattern}-d_{deltaX}-m_{window}-wp_{wPercent}"
            png_file = os.path.abspath(os.path.join(png_folder, filename + ".png"))
            eps_file = os.path.abspath(os.path.join(eps_folder, filename + ".eps"))
            #
            plt.savefig(png_file)
            plt.savefig(eps_file, format="eps")
            #
            #+++++++++++++++++++++++++++++++++++++++++++++++++++++++
            #
            #plt.show()
            plt.clf()
            plt.close()
            #
            #--------------
            e = elem
        #
        #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        #
    finally:
        #queue.put(mp.current_process().name)
        a = "finished"
#
#=====================================================================================
#=====================================================================================
#
if __name__ == "__main__":
    #
    configs = []
    for dataname in all_dataname:
        dataset = dataname + ".csv"
        for pattern in all_patterns:
        	for window in windowsSizes:
        	    for deltaX in all_deltaX[dataname]:
        	        for wPercent in all_epsilon:
        	            configs.append((dataname, dataset, pattern, window, deltaX, wPercent, lambda_val[0]))
    #
    curr = 0
    procs = dict()
    queue = mp.Queue() # queue of ongoing process
    for i in range(0, nbP):
        if curr < len(configs):
            pName = str(configs[curr][0]) + " * window=" + str(configs[curr][3])
            pName += " * dx=" + str(configs[curr][4]) + " * wp=" + str(configs[curr][5])
            proc = Process(name=pName, target=draw_graph, args=(queue, configs[curr], ))
            proc.start()
            procs[proc.name] = proc
            curr += 1
    # using the queue, launch a new process whenever an old process finishes its workload
    while procs:
        name = queue.get()
        proc = procs[name]
        print(proc) 
        proc.join()
        del procs[name]
        if curr < len(configs):
            pName = str(configs[curr][0]) + " * window=" + str(configs[curr][3])
            pName += " * dx=" + str(configs[curr][4]) + " * wp=" + str(configs[curr][5])
            proc = Process(name=pName, target=draw_graph, args=(queue, configs[curr], ))
            proc.start()
            procs[proc.name] = proc
            curr += 1
    #
    print("\n***** FINISHED *****\n")
    #
    #
#
# --------------------------------------------------------#
# --------------------------------------------------------#
#
