'''
Created on 23 Feb 2023

@author: Lobnury
'''
#
from include import *
#
timeout = "91800s" # or "86400s"
nbP = 4 # number of parallel executions
#
#
# --------------------------------------------------------#
# ---------------------- FUNCTIONS -----------------------#
# --------------------------------------------------------#
#
def has_numbers(s):
    return any(c.isdigit() for c in s)
#
#---------------------------------------------------------------
#
def has_letters(s):
    return any(c.isalpha() for c in s)
#
#---------------------------------------------------------------
#
def count_elem(data_csv, ids_elem):
    count = 0
    if (os.path.exists(data_csv)):
        with open(data_csv, 'r') as datFile:
            for line in datFile:
                line_ = line.strip().split(",")
                all_elem = "".join([ line_[i] for i in ids_elem ])
                #if not has_numbers(line_):
                if has_letters(all_elem):
                    continue
                count += 1
    return count
#
#---------------------------------------------------------------
#
def create_folder(folder):
    os.makedirs(folder,exist_ok=True)
#
# --------------------------------------------------------#
#
def save_to_csv(config, data, csv_filename, header=['inf', 'sup']):
    (dataname, mPattern, window, deltaX, wPercent, ov) = config
    path_to_csv = os.path.abspath(os.path.join(results_dir, 'anomalies-data', 'XP-10-05-2023', mPattern, dataname))
    path_to_csv = os.path.abspath(os.path.join(path_to_csv, 'window-'+str(window), 'deltaX-'+str(deltaX)))
    create_folder(path_to_csv)
    #
    path_to_csv = os.path.abspath(os.path.join(path_to_csv, csv_filename))
    #
    with open(path_to_csv, 'w', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(header)
        writer.writerows(data)
    #
#
#---------------------------------------------------------------
#
def from_window_to_X(nb_elem, window):
    #
    X = dict()
    #
    min_val = 0000
    #(min_val, nb_elem) = (50000, 52000)
    #
    for i in range(min_val, nb_elem):
    #for i in range(nb_elem):
        (inf, sup) = (i+1, i+window)
        if sup <= nb_elem:
            X[str(inf)+"-"+str(sup)] = inf
        #
    return X
    #
#
#---------------------------------------------------------------
#
def read_resFile(resFile):
    res_data = dict()
    if (os.path.exists(resFile)):
        with open(resFile, 'r') as resfile:
            for line in resfile:
                if not has_numbers(line):
                    continue
                #
                elem = line.strip().split(" ; ")
                if len(elem) == 3:
                    res_data[elem[0]] = float(elem[2])
                #
    return res_data
    #
#
#---------------------------------------------------------------
#
def parse_res_file(config):
    (dataname, mPattern, all_windows, deltaX, wPercent, ov) = config
    #
    val_by_windows = []
    for window in all_windows:
        res_dir = os.path.realpath(os.path.join(results_dir, "XP-10-05-2023", mPattern, dataname))
        res_dir = os.path.realpath(os.path.join(res_dir, "window-"+str(window), "deltaX-"+str(deltaX)))
        filename = dataname + "-wp_" + str(wPercent) + "-p_" + str(ov)
        res_file = os.path.join(res_dir, filename + ".log")
        #
        wData = read_resFile(res_file)
        val_by_windows.append(wData)
        #
    return val_by_windows
    #
#
#---------------------------------------------------------------
#
def draw_graph(queue, config):
    try:
        queue.put(mp.current_process().name)
        #
        (dataname, datafile, nb_elem, mPattern, feat, ag, all_windows, deltaX, wPercent, ov) = config
        #
        config_ = (dataname, mPattern, all_windows, deltaX, wPercent, ov)
        val_by_windows = parse_res_file(config_)
        #
        '''
        plt.figure(figsize=(12.0, 8.0))
        '''
        #
        for w in range(len(all_windows)):
            #
            x_dict = from_window_to_X(nb_elem, all_windows[w])
            y_dict = val_by_windows[w]
            #
            x = [ e for e in x_dict.values() ]
            #print(x_dict)
            y = [y_dict[e] if e in y_dict else 0.0 for e in x_dict.keys()]
            #
            x = [ e for e in x_dict.values() ]
            y = [y_dict[e] if (y_dict[e] >= wPercent) else 0.0 for e in x_dict.keys() ]
            #
            #x = [ e2 for (e1, e2) in zip(x_dict.keys(), x_dict.values()) if (y_dict[e1] >= wPercent)]
            #y = [y_dict[e] for e in x_dict.keys() if (y_dict[e] >= wPercent)]
            #
            #percent = " ~ " + "{:.3f}".format( 100*(len([e for e in y if e>wPercent])/len(y)) ) + "%"
            percent = " ~ " + "{:.3f}".format( 100*(len([e for e in y if e>wPercent])/len(y_dict.values())) ) + "%"
            #
            #print("\n", x[:10], "\n")
            onlyZeros = len([e for e in y if e==0.0]) == len(y)
            '''
            if not onlyZeros:
                plt.plot(x, y, color=all_colors[w], linestyle=all_linestyles[w], linewidth=1, label=all_labels[w] + percent)
            '''
            #
            #print(f"{x_dict.keys()}, {nb_elem}, {all_windows[w]}")
            data_to_csv = [ [e1, e1+all_windows[w]-1]  for (e1, e2) in zip(x, y) if e2 >= wPercent ]
            csv_filename = dataname + "-wp_" + str(wPercent) + "-p_" + str(ov) + ".csv"
            config_ = (dataname, mPattern, all_windows[w], deltaX, wPercent, ov)
            save_to_csv(config_, data_to_csv, csv_filename)
            #
        #plt.axhline(y=wPercent, color="black", linestyle="--")
        #
        #+++++++++++++++++++++++++++++++++++++++++++++++++++++++
        #
        '''
        plt.xlabel("Windows", fontsize=16)
        plt.ylabel("Percentage of Peak", fontsize=16)
        plt.title(f"Percentage of Peak for different window size - Min Threshold = {wPercent} ", fontsize=16)
        plt.legend(loc="best", fontsize=18)
        plt.tight_layout()
        #
        #+++++++++++++++++++++++++++++++++++++++++++++++++++++++
        #
        m_graphs_dir = os.path.abspath(os.path.join(graphs_dir, "XP-10-05-2023", "png", mPattern, dataname, "deltaX-"+str(deltaX)))
        m_graphs_dir_ = os.path.abspath(os.path.join(graphs_dir, "XP-10-05-2023", "eps", mPattern, dataname, "deltaX-"+str(deltaX)))
        create_folder(m_graphs_dir)
        create_folder(m_graphs_dir_)
        #
        filename = f"{dataname}-wp_{wPercent}-p_{ov}"
        png_file = os.path.abspath(os.path.join(m_graphs_dir, f"{filename}.png"))
        eps_file = os.path.abspath(os.path.join(m_graphs_dir_, f"{filename}.eps"))
        plt.savefig(png_file)
        plt.savefig(eps_file, format="eps")
        '''
        #
        #
    finally:
        #queue.put(mp.current_process().name)
        a = "finished"
#
#=====================================================================================
#=====================================================================================
#
if __name__ == "__main__":
    #
    datasets_lines = {
        "data-1" : [1], 
        "data-2" : [1],
        "iot_telemetry_data" : [1, 2], 
        "log_temp" : [1, 2], 
        "Occupancy" : [1, 2]
    }
    #
    #------------------------------------------------------------------------
    #
    # prepare the different configurations
    configs = []
    for dataname in all_dataname:
        dataset = dataname+".csv"
        data_file = os.path.join(data_dir, dataset)
        all_ids_elem = datasets_lines[dataname]
        nb_elem = count_elem(data_file, all_ids_elem)
        #nb_elem = 4000
        for pattern in all_patterns:
            for deltaX in all_deltaX[dataname]:
                for wp in all_epsilon:
                    for p in lambda_val:
                        configs.append((dataname, data_file, nb_elem, pattern, feature, aggreg, windowsSizes, deltaX, wp, p))
    #
    # launch processes one after the other, nbP processes can be launched in parallel
    curr = 0
    procs = dict()
    queue = mp.Queue() # queue of ongoing process
    for i in range(0, nbP):
        if curr < len(configs):
            pName = str(configs[curr][0]) + " * w=" + str(configs[curr][6]) + " * dx=" + str(configs[curr][7])
            pName += " * wp=" + str(configs[curr][8]) + " * p=" + str(configs[curr][9])
            proc = Process(name=pName, target=draw_graph, args=(queue, configs[curr], ))
            proc.start()
            procs[proc.name] = proc
            curr += 1
    # using the queue, launch a new process whenever an old process finishes its workload
    while procs:
        name = queue.get()
        proc = procs[name]
        print(proc) 
        proc.join()
        del procs[name]
        if curr < len(configs):
            pName = str(configs[curr][0]) + " * w=" + str(configs[curr][6]) + " * dx=" + str(configs[curr][7])
            pName += " * wp=" + str(configs[curr][8]) + " * p=" + str(configs[curr][9])
            proc = Process(name=pName, target=draw_graph, args=(queue, configs[curr], ))
            proc.start()
            procs[proc.name] = proc
            curr += 1
    #
    print("\n***** FINISHED *****\n")
    #
#
# --------------------------------------------------------#
# --------------------------------------------------------#
#

