# This Python 3 environment comes with many helpful analytics libraries installed
# It is defined by the kaggle/python Docker image: https://github.com/kaggle/docker-python

# --------------------------------------------------------#
# ----------------------- PACKAGES -----------------------#
# --------------------------------------------------------#
#
import numpy as np
import pandas as pd
import seaborn as sns
#
import matplotlib as mpl
import matplotlib.pyplot as plt # data visualization
import seaborn # statistical data visualization
#
import os
#
cwd = os.path.dirname(os.path.realpath(__file__)) # current directory
project_dir = os.path.realpath(os.path.join(cwd, os.pardir, os.pardir)) # root directory
data_dir = os.path.abspath(os.path.join(project_dir, "data"))
graphs_dir = os.path.abspath(os.path.join(project_dir, "graphs"))
#
# --------------------------------------------------------#
# ---------------------- FUNCTIONS -----------------------#
# --------------------------------------------------------#
#
def draw_sliding_window_graph():
    x = [i+1 for i in range(16)]
    y = [2, 1, 3, 3, 2, 1, 1, 2, 2, 3, 4, 4, 3, 1, 3, 3]
    s = ["<" if y[i]<y[i+1] else (">" if y[i]>y[i+1] else "=") for i in range(len(y)-1) ]
    #
    (x1, y1) = (x[1:6], y[1:6])
    (x2, y2) = (x[6:14], y[6:14])
    #
    x_str = list(map(str, x))
    y_str = list(map(str, list(set(y)) ) )
    #
    plt.figure(figsize=(12.0, 8.0))
    #
    plt.plot(x, y, marker="o", color="b", label="Time series X")
    plt.plot(x1, y1, marker="o", color="red", label=r"$1^{st}$ occ. of Peak")
    plt.plot(x2, y2, marker="o", color="green", label=r"$2^{nd}$ occ. of Peak")
    #
    for i in range(len(x)):
        plt.text(x[i]-0.05, y[i]+0.12, str(y[i]))
        #
        if i < len(x)-1:
            plt.text(((x[i]+x[i+1])/2)-0.075, 1.1-(float(1)/4), s[i])
    #
    lineText = ["3", "3", "*", "5", "6", "6", "6"]
    #lineText = ["8", "9", "7", "7"]
    textColor = ["red", "red", "black", "green", "green", "green", "green"]
    textColor = ["black", "black", "black", "black", "black", "black", "black"]
    #
    windowSize = 10
    #windowSize = 13
    #'''
    for i in range(0, len(x)-windowSize+1):
        plt.hlines(y=max(y)+(float(i+2)/4), xmin=i+1, xmax=i+windowSize, color="b", linestyle=":")
        plt.vlines(x=i+1, ymin=max(y)+(float(i+2)/4)-0.1, ymax=max(y)+(float(i+2)/4)+0.1, color="b")
        plt.vlines(x=i+windowSize, ymin=max(y)+(float(i+2)/4)-0.1, ymax=max(y)+(float(i+2)/4)+0.1, color="b")
        #
        plt.text(i+1-0.5, max(y)+(float(i+2)/4)-0.05, lineText[i], color=textColor[i], fontsize=14)
    #
    plt.hlines(y=max(y)+(float(2)/4), xmin=2, xmax=6, color="red")
    plt.hlines(y=max(y)+(float(3)/4), xmin=2, xmax=6, color="red")
    plt.hlines(y=max(y)+(float(5)/4), xmin=7, xmax=13, color="green")
    plt.hlines(y=max(y)+(float(6)/4), xmin=7, xmax=14, color="green")
    plt.hlines(y=max(y)+(float(7)/4), xmin=7, xmax=14, color="green")
    plt.hlines(y=max(y)+(float(8)/4), xmin=7, xmax=14, color="green")
    #
    plt.hlines(y=max(y)+(float(1)/4), xmin=1, xmax=16, color="b", linestyle=":")
    plt.vlines(x=1, ymin=max(y)+(float(1)/4)-0.1, ymax=max(y)+(float(1)/4)+0.1, color="b")
    plt.vlines(x=16, ymin=max(y)+(float(1)/4)-0.1, ymax=max(y)+(float(1)/4)+0.1, color="b")
    plt.hlines(y=max(y)+(float(1)/4), xmin=2, xmax=6, color="red")
    plt.hlines(y=max(y)+(float(1)/4), xmin=7, xmax=14, color="green")
    plt.text(1-0.5, max(y)+(float(1)/4)-0.05, "9", color="black", fontsize=14)
    #
    plt.xticks(x, x_str)
    plt.yticks(list(set(y)), y_str)
    plt.xlabel("Time", fontsize=16)
    plt.ylabel("Time series X", fontsize=16)
    plt.title("Length of Peaks", fontsize=16)
    #
    #plt.legend(fontsize=14)
    plt.legend(loc="upper left", fontsize=14)
    #plt.legend(bbox_to_anchor=(0.3, 0.75), fontsize=16)
    #
    plt.grid()
    plt.tight_layout()
    #
    png_file = os.path.abspath(os.path.join(graphs_dir, "graph.png"))
    eps_file = os.path.abspath(os.path.join(graphs_dir, "graph.eps"))
    plt.savefig(png_file)
    plt.savefig(eps_file, format="eps")
    #
    plt.show()
    plt.clf()
    plt.close()
    #
#
# --------------------------------------------------------#
# --------------------------------------------------------#
#
def seaborn_graph():
    sns.set_theme(style="ticks")
    #
    #data_file = os.path.abspath(os.path.join(data_dir, "penguins.csv"))
    #df = load_csv_data(data_file)
    #sns.pairplot(df, hue="species")
    #
    df = sns.load_dataset("tips")
    print(f"\n\n{df}\n\n")
    #
    sns.relplot(data=df, x="total_bill", y="tip", col="time",hue="smoker", style="smoker", size="size",)
    #
    #'''
    plt.show()
    plt.clf()
    plt.close()
    #'''
    #
#
# --------------------------------------------------------#
# --------------------------------------------------------#
#
def load_csv_data(path_to_csv):
    data_df = pd.read_csv(path_to_csv)
    #
    return data_df
    #
#
# --------------------------------------------------------#
# --------------------------------------------------------#
#
if __name__ == "__main__":
    #
    #draw_graph()
    seaborn_graph()
    #
#
# --------------------------------------------------------#
# --------------------------------------------------------#
#
