# This Python 3 environment comes with many helpful analytics libraries installed
#
# @Author : Lobnury
#
# --------------------------------------------------------#
# ----------------------- PACKAGES -----------------------#
# --------------------------------------------------------#
#
import os
import csv
from datetime import datetime
#
import numpy as np
import pandas as pd
import seaborn as sns
#
import matplotlib as mpl
import matplotlib.pyplot as plt # data visualization
import seaborn # statistical data visualization
#
textColor = ["red", "red", "black", "green", "green", "green", "green"]
all_datanames = ["AirPassengers", "penguins", "tips", "time-series", "time-series-upsampled"]
#
cwd = os.path.dirname(os.path.realpath(__file__)) # current directory
project_dir = os.path.realpath(os.path.join(cwd, os.pardir, os.pardir)) # root directory
data_dir = os.path.abspath(os.path.join(project_dir, "data"))
graphs_dir = os.path.abspath(os.path.join(project_dir, "graphs"))
#
# --------------------------------------------------------#
# ---------------------- FUNCTIONS -----------------------#
# --------------------------------------------------------#
#
#
def create_folder(folder):
    os.makedirs(folder,exist_ok=True)
#
#---------------------------------------------------------#
#
def load_csv_data(dataname):
    path_to_csv = os.path.abspath(os.path.join(data_dir, dataname))
    data_df = pd.read_csv(path_to_csv)
    return data_df
    #
#
# --------------------------------------------------------#
#
def save_to_csv(data, csv_filename, header=['floatvalue']):
    path_to_csv = os.path.abspath(os.path.join(data_dir, 'new', csv_filename))
    #
    with open(path_to_csv, 'w', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(header)
        writer.writerows(data)
    #
#
# --------------------------------------------------------#
#
def parse_date_time(datatime_list):
    result = []
    for elem in datatime_list:
        elem_list = elem.split(" ")
        date_elem = list(map(int, elem_list[0].split("-")))
        time_elem = list(map(int, elem_list[1].split(":")))
        #
        all_datetime_elem = date_elem + time_elem
        datetime_object = datetime(*all_datetime_elem)
        result.append(datetime_object)
        #
    return result
    #
#
# --------------------------------------------------------#
#
def lissage(x, y, p):
    x_new = [x[0]] + x[p : -p] + [x[-1]]
    y_new = [y[0]]
    for i in range(p, len(y)-p):
        avg_val = np.mean( y[i-p : i+p+1] ) # y[i-p : i+p] pour avoir un nombre pair de valeurs
        y_new.append(avg_val)
    #
    y_new = y_new + [y[-1]]
    #
    return (x_new, y_new)
#
# --------------------------------------------------------#
#
#def draw_sliding_window_graph():
def draw_graph():
    #
    dataname = "time-series-upsampled.csv"
    dataname = "data-2.csv"
    df = load_csv_data(dataname)
    #
    data_keys = df.keys()
    data_values = list(df['floatvalue'])
    #time_values = parse_date_time( list(df['timestamp']) )
    #
    (low, up, nbG) = (176000, 177000, 1)
    (low, up, nbG) = (125500, 125800, 1)
    (low, up, nbG) = (400000, 430000, 1)
    (low, up, nbG) = (940000, 958647, 1)
    #(low, up, nbG) = (800, 1400, 1)
    #(low, up, nbG) = (0000, len(data_values), 1)
    mStep = int((up-low)/nbG)
    p = 1
    #
    iter_val = [int(((i+1)*(up-low))/nbG)  for i in range(nbG)]
    e = low
    for i in range(len(iter_val)):
        elem = iter_val[i] + low
        nbVal = elem - e
        #nbVal = min(10000, len(data_values))
        #+++++++++++++++++++++++++++++++++++++++++++++++++++++++
        (low_, up_, mStep_) = (e, elem, int(mStep/10))
        all_x_ticks = [i for i in range(low_, up_+1, mStep_)]
        all_x_ticks_str = list(map(str, all_x_ticks))
        #+++++++++++++++++++++++++++++++++++++++++++++++++++++++
        y = data_values[e:elem]
        x = [e+k+1 for k in range(nbVal)]
        s = ["<" if y[k]<y[k+1] else (">" if y[k]>y[k+1] else "=") for k in range(nbVal-1) ]
        #--------------
        (x_new, y_new) = lissage(x, y, p)
        #+++++++++++++++++++++++++++++++++++++++++++++++++++++++
        x_str = list(map(str, x))
        y_str = list(map(str, list(set(y)) ) )
        #
        plt.figure(figsize=(12.0, 8.0))
        plt.plot(x, y, color="b", linewidth=1, label="Temperature")
        #plt.plot(x, y, marker=".", color="b", label="Time series X")
        #plt.plot(x_new, y_new, color="r", linewidth=1, label="Smouth Time series X")
        #+++++++++++++++++++++++++++++++++++++++++++++++++++++++
        plt.xticks(all_x_ticks, all_x_ticks_str, rotation=45)
        #plt.yticks(list(set(y)), y_str)
        plt.xlabel("Time", fontsize=16)
        plt.ylabel("Time series X", fontsize=16)
        #
        #plt.rcParams['text.usetex'] = True
        plt.title("Data-2", fontsize=16)
        #plt.title("Time series values", fontsize=16)
        plt.legend(loc="best", fontsize=18)
        #plt.legend(loc="upper left", fontsize=14)
        #plt.grid()
        plt.tight_layout()
        #+++++++++++++++++++++++++++++++++++++++++++++++++++++++
        create_folder( os.path.abspath(os.path.join(graphs_dir, "Analysis", "png")) )
        create_folder( os.path.abspath(os.path.join(graphs_dir, "Analysis", "eps")) )
        #
        png_file = os.path.abspath(os.path.join(graphs_dir, "Analysis", "png", f"time-series-{i+1}.png"))
        eps_file = os.path.abspath(os.path.join(graphs_dir, "Analysis", "eps", f"time-series-{i+1}.eps"))
        plt.savefig(png_file)
        plt.savefig(eps_file, format="eps")
        #+++++++++++++++++++++++++++++++++++++++++++++++++++++++
        plt.show()
        plt.clf()
        plt.close()
        #+++++++++++++++++++++++++++++++++++++++++++++++++++++++
        e = elem
        #+++++++++++++++++++++++++++++++++++++++++++++++++++++++
        new_dataname = os.path.splitext(os.path.basename(dataname))[0] + f"_NEW-{i+1}.csv"
        data_csv = [ list(dat) for dat in np.array([x_new, y_new]).transpose() ]
        #save_to_csv(data_csv, new_dataname, ['xValues', 'floatvalue'])
        #+++++++++++++++++++++++++++++++++++++++++++++++++++++++
        #
#
# --------------------------------------------------------#
# --------------------------------------------------------#
#
if __name__ == "__main__":
    #
    draw_graph()
    #
#
# --------------------------------------------------------#
# --------------------------------------------------------#
#
