/*
    The contents of this file are subject to the Mozilla Public License
    Version  1.1  (the "License"); you may not use this file except in
    compliance with the License. You may obtain a copy of the License at:

    http://www.mozilla.org/MPL/

    Software  distributed  under  the License is distributed on an "AS
    IS"  basis,  WITHOUT  WARRANTY  OF  ANY  KIND,  either  express or
    implied.  See  the  License  for  the  specific language governing
    rights and limitations under the License.
    The Original Code is the contents of this file.
    The Developer of the Original Code is IMT Atlantique
    (4 Rue Alfred Kastler, 44300 Nantes, France).
    Contributor(s):
    _____Arnold Hien <arnold.hien@imt-atlantique.fr>
*/

package fr.imtAtlantique.ls2n.tasc.timeSeries.functions.decorationTables;

import fr.imtAtlantique.ls2n.tasc.timeSeries.constants.Constants;
import fr.imtAtlantique.ls2n.tasc.timeSeries.constants.Semantics;
import fr.imtAtlantique.ls2n.tasc.timeSeries.functions.aggreg.Aggregator;
import fr.imtAtlantique.ls2n.tasc.timeSeries.functions.features.Features;
import fr.imtAtlantique.ls2n.tasc.timeSeries.transducers.Transducers;

public class Contribution {
	//
	public static double contribution(double[] X, byte aggreg, byte feature, byte pattern, boolean reverse){
		//
		int n = X.length;
		int m = n-1;
		//
		double C = Aggregator.default_g_f(aggreg, feature, X); // feature value of a found occurrence (found and found_e)
		double D = Features.neutral_f(feature); // feature value of a potential occurrence (maybe_b and maybe_b)
		double R = Aggregator.default_g_f(aggreg, feature, X); // aggregated values of the feature occurrence contribution
		//
		byte state = Transducers.STATE_s; // initial state
		byte after = Constants.get_after_value(pattern);
		//
		for(int k=0; k<m; k++) {
			//
			byte[] signature_and_semantic = Semantics.get_signature_and_semantic(X[k], X[k+1], pattern, state, reverse);
			byte semantic = signature_and_semantic[1];
			state = signature_and_semantic[2];
			//
			double[] accumulators = update_accumulators(k, k+1, X, D, C, R, 
					aggreg, feature, pattern, after, semantic);
			D = accumulators[0];
			C = accumulators[1];
			R = accumulators[2];
			
		}
		//
		return Aggregator.select_aggregator(aggreg, R, C);
	}
	/* 
	 * # ---------------------------------------------------------------------------------- #
	 * |                                                                                    |
	 * # ---------------------------------------------------------------------------------- #
	 */
	public static double[] contribution_Tab(double[] X, byte aggreg, byte feature, byte pattern, boolean reverse){
		//
		int n = X.length;
		int m = n-1;
		//
		double[] contribution = new double[n+1]; // contribution_and_end --> R
		//
		// Initialisations
		//double prv = Math.min(0, Features.neutral_f(feature)); // 28/02/2023;
		byte after = Constants.get_after_value(pattern);
		byte state = Transducers.STATE_s; // initial state
		double C = Aggregator.default_g_f(aggreg, feature, X); // feature value of a found occurrence (found and found_e)
		double D = Features.neutral_f(feature); // feature value of a potential occurrence (maybe_b and maybe_b)
		double R = Aggregator.default_g_f(aggreg, feature, X); // aggregated values of the feature occurrence contribution
		contribution[0] = R; // R[0] = r; --> contribution
		//
		for(int k=0; k<m; k++) {
			//
			byte[] signature_and_semantic = Semantics.get_signature_and_semantic(X[k], X[k+1], pattern, state, reverse);
			byte semantic = signature_and_semantic[1];
			state = signature_and_semantic[2];
			//
			double[] accumulators = update_accumulators(k, k+1, X, D, C, R, 
					aggreg, feature, pattern, after, semantic);
			D = accumulators[0];
			C = accumulators[1];
			R = accumulators[2];
			
			/*
			if((k+1 == m) && (semantic != Constants.OUT_a)) { // last semantic is not an OUT_a
				r = Aggregator.select_aggregator(aggreg, r, c);
				c = Aggregator.default_g_f(aggreg, feature, X);
			}
			// contribution_and_end[1][k+1] = r; // R[k+1] = r;
			contribution[k+1] = Aggregator.select_aggregator(aggreg, r, c); // 28/02/2023
			*/
			//
			// contribution[k+1] = R; // R[k+1] = r;
			contribution[k+1] = Aggregator.select_aggregator(aggreg, R, C); // 28/02/2023
		}
		//
		//--------------------------------------------------
		//
		contribution[n] = Aggregator.select_aggregator(aggreg, R, C); 
		//contribution[n] = r + c; // 28/02/2023
		
		
		return contribution;
	}
	/* 
	 * # ---------------------------------------------------------------------------------- #
	 * |                                                                                    |
	 * # ---------------------------------------------------------------------------------- #
	 */
	public static double[][] contribution_Tab(double[][] X, byte aggreg, byte feature, byte pattern, boolean reverse){
		//
		int nbMeasures = X.length;
		if(nbMeasures < 1)
			return null;
		int n = X[0].length;
		int m = n-1;
		//
		double[][] contribution = new double[nbMeasures][n+1]; // contribution_and_end --> R
		//
		// Initialisations
		byte after = Constants.get_after_value(pattern);
		byte[] state = new byte[nbMeasures];
		double[] C = new double[nbMeasures]; // feature value of a found occurrence (found and found_e)
		double[] D = new double[nbMeasures]; // feature value of a potential occurrence (maybe_b and maybe_b)
		double[] R = new double[nbMeasures]; // aggregated values of the feature occurrence contribution
		for(int i=0; i<nbMeasures; i++) {
			state[i] = Transducers.STATE_s; // initial state
			C[i] = Aggregator.default_g_f(aggreg, feature, X[i]);
			D[i] = Features.neutral_f(feature);
			R[i] = Aggregator.default_g_f(aggreg, feature, X[i]);
			contribution[i][0] = R[i]; // R[0] = r; --> contribution
		}
		//
		// Evaluate contribution
		//
		for(int i=0; i<nbMeasures; i++) {
			for(int k=0; k<m; k++) {
				//
				byte[] signature_and_semantic = 
						Semantics.get_signature_and_semantic(X[i][k], X[i][k+1], pattern, state[i], reverse);
				byte semantic = signature_and_semantic[1];
				state[i] = signature_and_semantic[2];
				//
				double[] accumulators = update_accumulators(k, k+1, X[i], D[i], C[i], R[i], 
						aggreg, feature, pattern, after, semantic);
				D[i] = accumulators[0];
				C[i] = accumulators[1];
				R[i] = accumulators[2];
				//
				// contribution[k+1] = R; // R[k+1] = r;
				contribution[i][k+1] = Aggregator.select_aggregator(aggreg, R[i], C[i]); // 28/02/2023
			}
			//
			//--------------------------------------------------
			//
			contribution[i][n] = Aggregator.select_aggregator(aggreg, R[i], C[i]); 
			//contribution[n] = r + c; // 28/02/2023
			
		}
		//
		return contribution;
	}
	/* 
	 * # ---------------------------------------------------------------------------------- #
	 * |                                                                                    |
	 * # ---------------------------------------------------------------------------------- #
	 */
	public static double[] update_accumulators(int k1, int k2, double[] X, double d, double c, double r, 
			byte aggreg, byte feature, byte pattern, byte after, byte semantic) {
		switch(semantic) { // semantic letter
			case Semantics.OUT:
				return accumulator_OUT(d, c, r, aggreg, feature, pattern, after);
			case Semantics.OUT_r:
				return accumulator_OUT_r(d, c, r, aggreg, feature, pattern, after);
			case Semantics.OUT_a:
				return accumulator_OUT_a(X, d, c, r, aggreg, feature, pattern, after);
			case Semantics.MAYBE_b:
				return accumulator_MAYBE_b(X[k1], X[k2], d, c, r, aggreg, feature, pattern, after);
			case Semantics.MAYBE_a:
				return accumulator_MAYBE_a(X[k1], X[k2], d, c, r, aggreg, feature, pattern, after);
			case Semantics.FOUND:
				return accumulator_FOUND(X[k1], X[k2], d, c, r, aggreg, feature, pattern, after);
			case Semantics.IN:
				return accumulator_IN(X[k1], X[k2], d, c, r, aggreg, feature, pattern, after);
			case Semantics.FOUND_e:
				return accumulator_FOUND_e(X[k1], X[k2], d, c, r, aggreg, feature, pattern, after);
			default:
				return accumulator_OUT(d, c, r, aggreg, feature, pattern, after);
		}
	}
	/* 
	 * # ---------------------------------------------------------------------------------- #
	 * |                                                                                    |
	 * # ---------------------------------------------------------------------------------- #
	 */
	public static double[] accumulator_OUT(double d, double c, double r, byte aggreg, byte feature, byte pattern, byte after) {
		return new double[] {d, c, r};
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static double[] accumulator_OUT_r(double d, double c, double r, byte aggreg, byte feature, byte pattern, byte after) {
		//
		double d_ = Features.neutral_f(feature);
		//
		return new double[] {d_, c, r};
		//
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static double[] accumulator_OUT_a(double[] X, double d, double c, double r, 
			byte aggreg, byte feature, byte pattern, byte after) {
		//
		double r_ = Aggregator.phi_g(aggreg, r, c);
		double c_ = Aggregator.default_g_f(aggreg, feature, X);
		double d_ = Features.neutral_f(feature);
		//
		return new double[] {d_, c_, r_};
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static double[] accumulator_MAYBE_b(double X_1, double X_2, double d, double c, double r, 
			byte aggreg, byte feature, byte pattern, byte after) {
		/*
		 *  X_1 SIGN X_2
		*/
		double d_ = Features.phi_f(feature, d, Features.delta_f(feature, X_1));
		//
		return new double[] {d_, c, r};
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static double[] accumulator_MAYBE_a(double X_1, double X_2, double d, double c, double r, 
			byte aggreg, byte feature, byte pattern, byte after) {
		/*
		 *  X_1 SIGN X_2
		*/
		double d_ = d;
		if(after == 0) {
			d_ = Features.phi_f(feature, d, Features.delta_f(feature, X_2));
		}
		else if(after == 1) {
			d_ = Features.phi_f(feature, d, Features.delta_f(feature, X_1));
		}
		//
		return new double[] {d_, c, r};
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static double[] accumulator_FOUND(double X_1, double X_2, double d, double c, double r, 
			byte aggreg, byte feature, byte pattern, byte after) {
		/*
		 *  X_1 SIGN X_2
		*/
		double d_ = d;
		double c_ = c;
		if(after == 0) {
			c_ = Features.phi_f(feature, 
					Features.phi_f(feature, d, Features.delta_f(feature, X_1)),
					Features.delta_f(feature, X_2));
			d_ = Features.neutral_f(feature);
		}
		else if(after == 1) {
			c_ = Features.phi_f(feature, d, Features.delta_f(feature, X_1));
			d_ = Features.neutral_f(feature);
		}
		//
		return new double[] {d_, c_, r};
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static double[] accumulator_IN(double X_1, double X_2, double d, double c, double r, 
			byte aggreg, byte feature, byte pattern, byte after) {
		/*
		 *  X_1 SIGN X_2
		*/
		double d_ = d;
		double c_ = c;
		if(after == 0) {
			c_ = Features.phi_f(feature, 
					c,
					Features.phi_f(feature, d, Features.delta_f(feature, X_2))
					);
			d_ = Features.neutral_f(feature);
		}
		else if(after == 1) {
			c_ = Features.phi_f(feature, 
					c,
					Features.phi_f(feature, d, Features.delta_f(feature, X_1))
					);
			d_ = Features.neutral_f(feature);
		}
		//
		return new double[] {d_, c_, r};
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static double[] accumulator_FOUND_e(double X_1, double X_2, double d, double c, double r, 
			byte aggreg, byte feature, byte pattern, byte after) {
		/*
		 *  X_1 SIGN X_2
		*/
		double d_ = d;
		double r_ = r;
		if(after == 0) {
			r_ = Aggregator.phi_g(aggreg, 
					r,
					Features.phi_f(feature, 
							Features.phi_f(feature, d, Features.delta_f(feature, X_1)), 
							Features.delta_f(feature, X_2))
					);
			d_ = Features.neutral_f(feature);
		}
		else if(after == 1) {
			r_ = Aggregator.phi_g(aggreg, 
					r,
					Features.phi_f(feature, d, Features.delta_f(feature, X_1))
					);
			d_ = Features.neutral_f(feature);
		}
		//
		return new double[] {d_, c, r_};
	}
	
}


//
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
//
