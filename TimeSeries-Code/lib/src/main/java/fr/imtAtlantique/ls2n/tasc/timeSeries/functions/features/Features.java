/*
    The contents of this file are subject to the Mozilla Public License
    Version  1.1  (the "License"); you may not use this file except in
    compliance with the License. You may obtain a copy of the License at:

    http://www.mozilla.org/MPL/

    Software  distributed  under  the License is distributed on an "AS
    IS"  basis,  WITHOUT  WARRANTY  OF  ANY  KIND,  either  express or
    implied.  See  the  License  for  the  specific language governing
    rights and limitations under the License.
    The Original Code is the contents of this file.
    The Developer of the Original Code is IMT Atlantique
    (4 Rue Alfred Kastler, 44300 Nantes, France).
    Contributor(s):
    _____Arnold Hien <arnold.hien@imt-atlantique.fr>
*/

package fr.imtAtlantique.ls2n.tasc.timeSeries.functions.features;

public class Features {
	/*
	 * #-----------------------------------------------------------------#
	 * |                           All Features                          |
	 * #-----------------------------------------------------------------#
	 */
	public static final byte Max_f = 1; // the max value of the occurrence of the pattern
	public static final byte Min_f = 2; // the min value of the occurrence of the pattern
	public static final byte One_f = 3; // set 1 at the position of each digit of the occurrence of the pattern
	public static final byte Range_f = 4; // 
	public static final byte Surface_f = 5; // the sum of the values of the occurrence of the pattern
	public static final byte Width_f = 6; // the length of the occurrence of the pattern
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static double neutral_f = -1;
	public static double min_f = -1;
	public static double max_f = -1;
	public static double phi_f = -1;
	public static double delta_f = -1;
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static double neutral_f() {
		return -1;
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static double min_f(double ... vals) {
		return -1;
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static double max_f(double ... vals) {
		return -1;
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static double phi_f(double ... vals) {
		return -1;
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static double delta_f(double ... vals) {
		return -1;
	}
	/*
	 * @-----------------------------------------------------------------@
	 * @-----------------------------------------------------------------@
	 * @-----------------------------------------------------------------@
	 */
	public static double neutral_f(byte feature) {
		switch(feature) {
			case Max_f:
				return Max.neutral_f();
			case Min_f:
				return Min.neutral_f();
			case One_f:
				return One.neutral_f();
			case Range_f:
				return Range.neutral_f();
			case Surface_f:
				return Surface.neutral_f();
			case Width_f:
				return Width.neutral_f();
			default:
				return neutral_f();
		}
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static double min_f(byte feature, double ... vals) {
		switch(feature) {
			case Max_f:
				return Max.min_f(vals);
			case Min_f:
				return Min.min_f(vals);
			case One_f:
				return One.min_f(vals);
			case Range_f:
				return Range.min_f(vals);
			case Surface_f:
				return Surface.min_f(vals);
			case Width_f:
				return Width.min_f(vals);
			default:
				return min_f(vals);
		}
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static double max_f(byte feature, double ... vals) {
		switch(feature) {
			case Max_f:
				return Max.max_f(vals);
			case Min_f:
				return Min.max_f(vals);
			case One_f:
				return One.max_f(vals);
			case Range_f:
				return Range.max_f(vals);
			case Surface_f:
				return Surface.max_f(vals);
			case Width_f:
				return Width.max_f(vals);
			default:
				return max_f(vals);
		}
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static double phi_f(byte feature, double ... vals) {
		switch(feature) {
			case Max_f:
				return Max.phi_f(vals);
			case Min_f:
				return Min.phi_f(vals);
			case One_f:
				return One.phi_f(vals);
			case Range_f:
				return Range.phi_f(vals);
			case Surface_f:
				return Surface.phi_f(vals);
			case Width_f:
				return Width.phi_f(vals);
			default:
				return phi_f(vals);
		}
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static double delta_f(byte feature, double ... vals) {
		switch(feature) {
			case Max_f:
				return Max.delta_f(vals);
			case Min_f:
				return Min.delta_f(vals);
			case One_f:
				return One.delta_f(vals);
			case Range_f:
				return Range.delta_f(vals);
			case Surface_f:
				return Surface.delta_f(vals);
			case Width_f:
				return Width.delta_f(vals);
			default:
				return delta_f(vals);
		}
	}
	/*
	 * @-----------------------------------------------------------------@
	 * @-----------------------------------------------------------------@
	 * @-----------------------------------------------------------------@
	 */
	public static double max_tab(double ... vals) {
		double max = Float.NEGATIVE_INFINITY;
		for(int i=0; i<vals.length; i++)
			max = Math.max(max, vals[i]);
		return max;
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static double min_tab(double ... vals) {
		double min = Float.POSITIVE_INFINITY;
		for(int i=0; i<vals.length; i++)
			min = Math.min(min, vals[i]);
		return min;
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static double sum_tab(double ... vals) {
		double sum = 0;
		for(int i=0; i<vals.length; i++)
			sum += vals[i];
		return sum;
	}
	
}
