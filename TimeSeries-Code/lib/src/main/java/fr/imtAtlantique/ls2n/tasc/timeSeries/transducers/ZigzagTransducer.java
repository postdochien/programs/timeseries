/*
    The contents of this file are subject to the Mozilla Public License
    Version  1.1  (the "License"); you may not use this file except in
    compliance with the License. You may obtain a copy of the License at:

    http://www.mozilla.org/MPL/

    Software  distributed  under  the License is distributed on an "AS
    IS"  basis,  WITHOUT  WARRANTY  OF  ANY  KIND,  either  express or
    implied.  See  the  License  for  the  specific language governing
    rights and limitations under the License.
    The Original Code is the contents of this file.
    The Developer of the Original Code is IMT Atlantique
    (4 Rue Alfred Kastler, 44300 Nantes, France).
    Contributor(s):
    _____Arnold Hien <arnold.hien@imt-atlantique.fr>
*/

package fr.imtAtlantique.ls2n.tasc.timeSeries.transducers;

import fr.imtAtlantique.ls2n.tasc.timeSeries.constants.Alphabet;
import fr.imtAtlantique.ls2n.tasc.timeSeries.constants.Semantics;

public class ZigzagTransducer {
	
	public static byte[] getTransition(byte currentState, byte letter) {
		// output --> new state and semantic letter
		byte[] output = { -1, -1 }; // output[0] --> new state, output[0] --> semantic letter
		
		switch(currentState) {
			case Transducers.STATE_s:
				if(letter == Alphabet.LESSER) {
					output[0] = Transducers.STATE_a;
					output[1] = Semantics.OUT;
				} else if(letter == Alphabet.GREATER) {
					output[0] = Transducers.STATE_d;
					output[1] = Semantics.OUT;
				} else {
					output[0] = currentState;
					output[1] = Semantics.OUT;
				}
				break;
			case Transducers.STATE_a:
				if(letter == Alphabet.LESSER) {
					output[0] = currentState;
					output[1] = Semantics.OUT;
				} else if(letter == Alphabet.GREATER) {
					output[0] = Transducers.STATE_b;
					output[1] = Semantics.MAYBE_b;
				} else {
					output[0] = Transducers.STATE_s;
					output[1] = Semantics.OUT;
				}
				break;
			case Transducers.STATE_b:
				if(letter == Alphabet.LESSER) {
					output[0] = Transducers.STATE_c;
					output[1] = Semantics.FOUND;
				} else if(letter == Alphabet.GREATER) {
					output[0] = Transducers.STATE_d;
					output[1] = Semantics.OUT_r;
				} else {
					output[0] = Transducers.STATE_s;
					output[1] = Semantics.OUT_r;
				}
				break;
			case Transducers.STATE_c:
				if(letter == Alphabet.LESSER) {
					output[0] = Transducers.STATE_a;
					output[1] = Semantics.OUT_a;
				} else if(letter == Alphabet.GREATER) {
					output[0] = Transducers.STATE_f;
					output[1] = Semantics.IN;
				} else {
					output[0] = Transducers.STATE_s;
					output[1] = Semantics.OUT_a;
				}
				break;
			case Transducers.STATE_d:
				if(letter == Alphabet.GREATER) {
					output[0] = currentState;
					output[1] = Semantics.OUT;
				} else if(letter == Alphabet.LESSER) {
					output[0] = Transducers.STATE_e;
					output[1] = Semantics.MAYBE_b;
				} else {
					output[0] = Transducers.STATE_s;
					output[1] = Semantics.OUT;
				}
				break;
			case Transducers.STATE_e:
				if(letter == Alphabet.LESSER) {
					output[0] = Transducers.STATE_a;
					output[1] = Semantics.OUT_r;
				} else if(letter == Alphabet.GREATER) {
					output[0] = Transducers.STATE_f;
					output[1] = Semantics.FOUND;
				} else {
					output[0] = Transducers.STATE_s;
					output[1] = Semantics.OUT_r;
				}
				break;
			case Transducers.STATE_f:
				if(letter == Alphabet.LESSER) {
					output[0] = Transducers.STATE_c;
					output[1] = Semantics.IN;
				} else if(letter == Alphabet.GREATER) {
					output[0] = Transducers.STATE_d;
					output[1] = Semantics.OUT_a;
				} else {
					output[0] = Transducers.STATE_s;
					output[1] = Semantics.OUT_a;
				}
				break;
		}
		return output;
	}
	
	/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	
	public static byte[] getReverseTransition(byte currentState, byte letter) {
		return getTransition(currentState, letter);
	}
	
	/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	
	public static byte[] getReverseTransition_(byte currentState, byte letter) {
		// output --> new state and semantic letter
		byte[] output = { -1, -1 }; // output[0] --> new state, output[0] --> semantic letter
		
		switch(currentState) {
			case Transducers.STATE_s:
				if(letter == Alphabet.GREATER) {
					output[0] = Transducers.STATE_a;
					output[1] = Semantics.OUT;
				} else if(letter == Alphabet.LESSER) {
					output[0] = Transducers.STATE_d;
					output[1] = Semantics.OUT;
				} else {
					output[0] = currentState;
					output[1] = Semantics.OUT;
				}
				break;
			case Transducers.STATE_a:
				if(letter == Alphabet.GREATER) {
					output[0] = currentState;
					output[1] = Semantics.OUT;
				} else if(letter == Alphabet.LESSER) {
					output[0] = Transducers.STATE_b;
					output[1] = Semantics.MAYBE_b;
				} else {
					output[0] = Transducers.STATE_s;
					output[1] = Semantics.OUT;
				}
				break;
			case Transducers.STATE_b:
				if(letter == Alphabet.GREATER) {
					output[0] = Transducers.STATE_c;
					output[1] = Semantics.FOUND;
				} else if(letter == Alphabet.LESSER) {
					output[0] = Transducers.STATE_d;
					output[1] = Semantics.OUT_r;
				} else {
					output[0] = Transducers.STATE_s;
					output[1] = Semantics.OUT_r;
				}
				break;
			case Transducers.STATE_c:
				if(letter == Alphabet.GREATER) {
					output[0] = Transducers.STATE_a;
					output[1] = Semantics.OUT_a;
				} else if(letter == Alphabet.LESSER) {
					output[0] = Transducers.STATE_f;
					output[1] = Semantics.IN;
				} else {
					output[0] = Transducers.STATE_s;
					output[1] = Semantics.OUT_a;
				}
				break;
			case Transducers.STATE_d:
				if(letter == Alphabet.LESSER) {
					output[0] = currentState;
					output[1] = Semantics.OUT;
				} else if(letter == Alphabet.GREATER) {
					output[0] = Transducers.STATE_e;
					output[1] = Semantics.MAYBE_b;
				} else {
					output[0] = Transducers.STATE_s;
					output[1] = Semantics.OUT;
				}
				break;
			case Transducers.STATE_e:
				if(letter == Alphabet.GREATER) {
					output[0] = Transducers.STATE_a;
					output[1] = Semantics.OUT_r;
				} else if(letter == Alphabet.LESSER) {
					output[0] = Transducers.STATE_f;
					output[1] = Semantics.FOUND;
				} else {
					output[0] = Transducers.STATE_s;
					output[1] = Semantics.OUT_r;
				}
				break;
			case Transducers.STATE_f:
				if(letter == Alphabet.GREATER) {
					output[0] = Transducers.STATE_c;
					output[1] = Semantics.IN;
				} else if(letter == Alphabet.LESSER) {
					output[0] = Transducers.STATE_d;
					output[1] = Semantics.OUT_a;
				} else {
					output[0] = Transducers.STATE_s;
					output[1] = Semantics.OUT_a;
				}
				break;
		}
		return output;
	}
	
}
