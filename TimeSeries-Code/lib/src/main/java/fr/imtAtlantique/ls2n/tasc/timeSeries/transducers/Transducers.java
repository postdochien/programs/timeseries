/*
    The contents of this file are subject to the Mozilla Public License
    Version  1.1  (the "License"); you may not use this file except in
    compliance with the License. You may obtain a copy of the License at:

    http://www.mozilla.org/MPL/

    Software  distributed  under  the License is distributed on an "AS
    IS"  basis,  WITHOUT  WARRANTY  OF  ANY  KIND,  either  express or
    implied.  See  the  License  for  the  specific language governing
    rights and limitations under the License.
    The Original Code is the contents of this file.
    The Developer of the Original Code is IMT Atlantique
    (4 Rue Alfred Kastler, 44300 Nantes, France).
    Contributor(s):
    _____Arnold Hien <arnold.hien@imt-atlantique.fr>
*/

package fr.imtAtlantique.ls2n.tasc.timeSeries.transducers;

import fr.imtAtlantique.ls2n.tasc.timeSeries.constants.Constants;

public class Transducers {
	/*
	 * #-----------------------------------------------------------------#
	 * |                             States                              |
	 * #-----------------------------------------------------------------#
	 */
	public static final byte STATE_s = 0; // initial state
	public static final byte STATE_r = 1; // state r
	public static final byte STATE_t = 2; // state t
	public static final byte STATE_u = 3; // state u
	public static final byte STATE_v = 4; // state v
	public static final byte STATE_a = 5; // state a
	public static final byte STATE_b = 6; // state b
	public static final byte STATE_c = 7; // state c
	public static final byte STATE_d = 8; // state d
	public static final byte STATE_e = 9; // state e
	public static final byte STATE_f = 10; // state f
	public static final byte initial_state = STATE_s;
	/*
	 * #-----------------------------------------------------------------#
	 * |                      Patterns Transducer                        |
	 * #-----------------------------------------------------------------#
	 */
	public static byte[] selectTransducer(byte pattern, byte currentState, byte letter) {
		switch(pattern){
			case Constants.Inflexion:
				return InflexionTransducer.getTransition(currentState, letter);
			case Constants.BumpOnDecSeq:
				return BumpOnDecSeqTransducer.getTransition(currentState, letter);
			case Constants.DipOnIncSeq:
				return DipOnIncSeqTransducer.getTransition(currentState, letter);
			case Constants.Dec:
				return DecTransducer.getTransition(currentState, letter);
			case Constants.Inc:
				return IncTransducer.getTransition(currentState, letter);
			case Constants.Steady:
				return SteadyTransducer.getTransition(currentState, letter);
			case Constants.DecTerrace:
				return DecTerraceTransducer.getTransition(currentState, letter);
			case Constants.IncTerrace:
				return IncTerraceTransducer.getTransition(currentState, letter);
			case Constants.Plain:
				return PlainTransducer.getTransition(currentState, letter);
			case Constants.Plateau:
				return PlateauTransducer.getTransition(currentState, letter);
			case Constants.ProperPlain:
				return ProperPlainTransducer.getTransition(currentState, letter);
			case Constants.ProperPlateau:
				return ProperPlateauTransducer.getTransition(currentState, letter);
			case Constants.Gorge:
				return GorgeTransducer.getTransition(currentState, letter);
			case Constants.Summit:
				return SummitTransducer.getTransition(currentState, letter);
			case Constants.Peak:
				return PeakTransducer.getTransition(currentState, letter);
			case Constants.Valley:
				return ValleyTransducer.getTransition(currentState, letter);
			case Constants.DecSeq:
				return DecSeqTransducer.getTransition(currentState, letter);
			case Constants.IncSeq:
				return IncSeqTransducer.getTransition(currentState, letter);
			case Constants.SteadySeq:
				return SteadySeqTransducer.getTransition(currentState, letter);
			case Constants.StrictlyDecSeq:
				return StrictlyDecSeqTransducer.getTransition(currentState, letter);
			case Constants.StrictlyIncSeq:
				return StrictlyIncSeqTransducer.getTransition(currentState, letter);
			case Constants.Zigzag:
				return ZigzagTransducer.getTransition(currentState, letter);
			case Constants.TempHumidDec:
				return TempHumidDecTransducer.getTransition(currentState, letter);
			case Constants.TempHumidInc:
				return TempHumidIncTransducer.getTransition(currentState, letter);
			default:
				return PeakTransducer.getTransition(currentState, letter);
		}
	}
	/*
	 * #-----------------------------------------------------------------#
	 * |                  Reverse Patterns Transducer                    |
	 * #-----------------------------------------------------------------#
	 */
	public static byte[] selectReverseTransducer(byte pattern, byte currentState, byte letter) {
		switch(pattern){
			case Constants.Inflexion:
				return InflexionTransducer.getReverseTransition(currentState, letter);
			case Constants.BumpOnDecSeq:
				return BumpOnDecSeqTransducer.getReverseTransition(currentState, letter);
			case Constants.DipOnIncSeq:
				return DipOnIncSeqTransducer.getReverseTransition(currentState, letter);
			case Constants.Dec:
				return DecTransducer.getReverseTransition(currentState, letter);
			case Constants.Inc:
				return IncTransducer.getReverseTransition(currentState, letter);
			case Constants.Steady:
				return SteadyTransducer.getReverseTransition(currentState, letter);
			case Constants.DecTerrace:
				return DecTerraceTransducer.getReverseTransition(currentState, letter);
			case Constants.IncTerrace:
				return IncTerraceTransducer.getReverseTransition(currentState, letter);
			case Constants.Plain:
				return PlainTransducer.getReverseTransition(currentState, letter);
			case Constants.Plateau:
				return PlateauTransducer.getReverseTransition(currentState, letter);
			case Constants.ProperPlain:
				return ProperPlainTransducer.getReverseTransition(currentState, letter);
			case Constants.ProperPlateau:
				return ProperPlateauTransducer.getReverseTransition(currentState, letter);
			case Constants.Gorge:
				return GorgeTransducer.getReverseTransition(currentState, letter);
			case Constants.Summit:
				return SummitTransducer.getReverseTransition(currentState, letter);
			case Constants.Peak:
				return PeakTransducer.getReverseTransition(currentState, letter);
			case Constants.Valley:
				return ValleyTransducer.getReverseTransition(currentState, letter);
			case Constants.DecSeq:
				return DecSeqTransducer.getReverseTransition(currentState, letter);
			case Constants.IncSeq:
				return IncSeqTransducer.getReverseTransition(currentState, letter);
			case Constants.SteadySeq:
				return SteadySeqTransducer.getReverseTransition(currentState, letter);
			case Constants.StrictlyDecSeq:
				return StrictlyDecSeqTransducer.getReverseTransition(currentState, letter);
			case Constants.StrictlyIncSeq:
				return StrictlyIncSeqTransducer.getReverseTransition(currentState, letter);
			case Constants.Zigzag:
				return ZigzagTransducer.getReverseTransition(currentState, letter);
			case Constants.TempHumidDec:
				return TempHumidDecTransducer.getReverseTransition(currentState, letter);
			case Constants.TempHumidInc:
				return TempHumidIncTransducer.getReverseTransition(currentState, letter);
			default:
				return PeakTransducer.getReverseTransition(currentState, letter);
		}
	}
}
