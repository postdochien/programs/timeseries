/*
    The contents of this file are subject to the Mozilla Public License
    Version  1.1  (the "License"); you may not use this file except in
    compliance with the License. You may obtain a copy of the License at:

    http://www.mozilla.org/MPL/

    Software  distributed  under  the License is distributed on an "AS
    IS"  basis,  WITHOUT  WARRANTY  OF  ANY  KIND,  either  express or
    implied.  See  the  License  for  the  specific language governing
    rights and limitations under the License.
    The Original Code is the contents of this file.
    The Developer of the Original Code is IMT Atlantique
    (4 Rue Alfred Kastler, 44300 Nantes, France).
    Contributor(s):
    _____Arnold Hien <arnold.hien@imt-atlantique.fr>
*/

package fr.imtAtlantique.ls2n.tasc.timeSeries.functions.decorationTables;

import java.util.LinkedList;

import fr.imtAtlantique.ls2n.tasc.timeSeries.constants.Constants;
import fr.imtAtlantique.ls2n.tasc.timeSeries.constants.Semantics;
import fr.imtAtlantique.ls2n.tasc.timeSeries.transducers.Transducers;

public class EndOfPatterns {
	//
	public static int[] endOfPatterns(double[] X, byte pattern, boolean reverse) {
		int n = X.length; // the length of the sequence
		int m = n-1;
		//
		int found = (n+1);
		int[] In = new int[n];
		int[] Ma = new int[n];
		int[] End = new int[n+1];
		byte state_ = Transducers.STATE_s; // initial state
		byte case_ = Constants.get_case(pattern); // CASE_FOUND OR CASE_FOUND_e
		LinkedList<Integer> list_of_ends = new LinkedList<Integer>(); // Ends of occurrence
		//
		// Initialisations
		End[0] = -1;
		In[0] = Ma[0] = 0;
		//
		// first loop to set links and some of the variable end's values
		for(int i=0; i<m; i++) {
			//
			byte[] signature_and_semantic = Semantics.get_signature_and_semantic(X[i], X[i+1], pattern, state_, reverse);
			byte semantic = signature_and_semantic[1];
			state_ = signature_and_semantic[2];
			//
			int[] vals = update_endOfPatterns(case_, semantic, i, End, In, Ma, found, list_of_ends);
			End[i+1] = vals[0];
			In[i+1] = vals[1];
			Ma[i] = vals[2];
			Ma[i+1] = vals[3];
			found = vals[4];
		}
		//
		if(case_ == Constants.CASE_FOUND) {
			if(found != n+1) {
				list_of_ends.add(found);
				found = (n+1);
			}
			if(In[n-1] <= 1) // the last semantic letter is not a MAYBE_a
				End[n-1] = Ma[n-1] = (n+1 - In[n-1]);
		}
		
		// default value for End[n]
		End[n] = (n+1);
		list_of_ends.add(End[n]); // list_of_ends.add(End[n-1]);
		//
		// second loop to set all End values previously set to -1
		int next_end = list_of_ends.pop();
		for(int i=0; i<n; i++) {
			if(i < next_end)
				End[i] = next_end;
			if(i+1 == next_end) 
				next_end = list_of_ends.pop();
		}
		//
		return End;
		
	}
	/* 
	 * # ---------------------------------------------------------------------------------- #
	 * |                                                                                    |
	 * # ---------------------------------------------------------------------------------- #
	 */
	public static int[] update_endOfPatterns(byte case_, byte semantic, int i, int[] End, int[] In, int[] Ma, 
			int found, LinkedList<Integer> ends_of_occurrence) {
		int[] vals = null;
		switch(semantic) { // semantic letter
			case Semantics.OUT:
				vals = endOfPatterns_OUT(i, End, In, Ma, found, ends_of_occurrence, case_);
				break;
			case Semantics.OUT_r:
				vals = endOfPatterns_OUT_r(i, End, In, Ma, found, ends_of_occurrence, case_);
				break;
			case Semantics.OUT_a:
				vals = endOfPatterns_OUT_a(i, End, In, Ma, found, ends_of_occurrence);
				break;
			case Semantics.MAYBE_b:
				vals = endOfPatterns_MAYBE_b(i, End, In, Ma, found, ends_of_occurrence, case_);
				break;
			case Semantics.MAYBE_a:
				vals = endOfPatterns_MAYBE_a(i, End, In, Ma, found, ends_of_occurrence);
				break;
			case Semantics.FOUND:
				vals = endOfPatterns_FOUND(i, End, In, Ma, found, ends_of_occurrence);
				break;
			case Semantics.IN:
				vals = endOfPatterns_IN(i, End, In, Ma, found, ends_of_occurrence);
				break;
			case Semantics.FOUND_e:
				vals = endOfPatterns_FOUND_e(i, End, In, Ma, found, ends_of_occurrence);
				break;
		}
		//
		return vals;
	}
	//
	public static double[] update_endOfPatterns(byte case_, byte semantic, int i, double[] End, double[] In, double[] Ma, 
			double found, LinkedList<Double> ends_of_occurrence) {
		double[] vals = null;
		switch(semantic) { // semantic letter
			case Semantics.OUT:
				vals = endOfPatterns_OUT(i, End, In, Ma, found, ends_of_occurrence, case_);
				break;
			case Semantics.OUT_r:
				vals = endOfPatterns_OUT_r(i, End, In, Ma, found, ends_of_occurrence, case_);
				break;
			case Semantics.OUT_a:
				vals = endOfPatterns_OUT_a(i, End, In, Ma, found, ends_of_occurrence);
				break;
			case Semantics.MAYBE_b:
				vals = endOfPatterns_MAYBE_b(i, End, In, Ma, found, ends_of_occurrence, case_);
				break;
			case Semantics.MAYBE_a:
				vals = endOfPatterns_MAYBE_a(i, End, In, Ma, found, ends_of_occurrence);
				break;
			case Semantics.FOUND:
				vals = endOfPatterns_FOUND(i, End, In, Ma, found, ends_of_occurrence);
				break;
			case Semantics.IN:
				vals = endOfPatterns_IN(i, End, In, Ma, found, ends_of_occurrence);
				break;
			case Semantics.FOUND_e:
				vals = endOfPatterns_FOUND_e(i, End, In, Ma, found, ends_of_occurrence);
				break;
		}
		//
		return vals;
	}
	/* 
	 * # ---------------------------------------------------------------------------------- #
	 * |                                                                                    |
	 * # ---------------------------------------------------------------------------------- #
	 */
	public static int[] endOfPatterns_OUT(int i, int[] End, int[] In, int[] Ma, int found, 
			LinkedList<Integer> ends_of_occurrence, byte case_){
		//
		int[] var = null;
		if(case_ == Constants.CASE_FOUND) {
			var = new int[] {-1, 0, Ma[i], 0, found};
		}
		else if(case_ == Constants.CASE_FOUND_e) {
			var = new int[] {-1, In[i+1], Ma[i], Ma[i+1], found};
		}
		//
		return var; // var : {End[i+1], In[i+1], Ma[i], Ma[i+1], found}
	}
	//
	public static double[] endOfPatterns_OUT(int i, double[] End, double[] In, double[] Ma, double found, 
			LinkedList<Double> ends_of_occurrence, byte case_){
		//
		double[] var = null;
		if(case_ == Constants.CASE_FOUND) {
			var = new double[] {-1, 0, Ma[i], 0, found};
		}
		else if(case_ == Constants.CASE_FOUND_e) {
			var = new double[] {-1, In[i+1], Ma[i], Ma[i+1], found};
		}
		//
		return var; // var : {End[i+1], In[i+1], Ma[i], Ma[i+1], found}
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static int[] endOfPatterns_OUT_r(int i, int[] End, int[] In, int[] Ma, int found, 
			LinkedList<Integer> ends_of_occurrence, byte case_){
		//
		int[] var = null;
		if(case_ == Constants.CASE_FOUND) {
			var = new int[] {-1, 0, Ma[i], 0, found};
		}
		else if(case_ == Constants.CASE_FOUND_e) {
			var = new int[] {-1, In[i+1], Ma[i], Ma[i+1], found};
		}
		//
		return var; // var : {End[i+1], In[i+1], Ma[i], Ma[i+1], found}
	}
	//
	public static double[] endOfPatterns_OUT_r(int i, double[] End, double[] In, double[] Ma, double found, 
			LinkedList<Double> ends_of_occurrence, byte case_){
		//
		double[] var = null;
		if(case_ == Constants.CASE_FOUND) {
			var = new double[] {-1, 0, Ma[i], 0, found};
		}
		else if(case_ == Constants.CASE_FOUND_e) {
			var = new double[] {-1, In[i+1], Ma[i], Ma[i+1], found};
		}
		//
		return var; // var : {End[i+1], In[i+1], Ma[i], Ma[i+1], found}
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static int[] endOfPatterns_OUT_a(int i, int[] End, int[] In, int[] Ma, int found, 
			LinkedList<Integer> ends_of_occurrence){
		//
		// In.length : sequence length
		int[] var = new int[] {-1, 0, (i + 2 - In[i]), 0, (In.length+1)};
		ends_of_occurrence.add((i + 2 - In[i]));
		//
		return var; // var : {End[i+1], In[i+1], Ma[i], Ma[i+1], found}
	}
	//
	public static double[] endOfPatterns_OUT_a(int i, double[] End, double[] In, double[] Ma, double found, 
			LinkedList<Double> ends_of_occurrence){
		//
		// In.length : sequence length
		double[] var = new double[] {-1, 0, (i + 2 - In[i]), 0, (In.length+1)};
		ends_of_occurrence.add((double) (i + 2 - In[i]));
		//
		return var; // var : {End[i+1], In[i+1], Ma[i], Ma[i+1], found}
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static int[] endOfPatterns_MAYBE_b(int i, int[] End, int[] In, int[] Ma, int found, 
			LinkedList<Integer> ends_of_occurrence, byte case_){
		//
		int[] var = null;
		if(case_ == Constants.CASE_FOUND) {
			var = new int[] {-1, 0, Ma[i], 0, found};
		}
		else if(case_ == Constants.CASE_FOUND_e) {
			var = new int[] {-1, In[i+1], Ma[i], Ma[i+1], found};
		}
		//
		return var; // var : {End[i+1], In[i+1], Ma[i], Ma[i+1], found}
	}
	//
	public static double[] endOfPatterns_MAYBE_b(int i, double[] End, double[] In, double[] Ma, double found, 
			LinkedList<Double> ends_of_occurrence, byte case_){
		//
		double[] var = null;
		if(case_ == Constants.CASE_FOUND) {
			var = new double[] {-1, 0, Ma[i], 0, found};
		}
		else if(case_ == Constants.CASE_FOUND_e) {
			var = new double[] {-1, In[i+1], Ma[i], Ma[i+1], found};
		}
		//
		return var; // var : {End[i+1], In[i+1], Ma[i], Ma[i+1], found}
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static int[] endOfPatterns_MAYBE_a(int i, int[] End, int[] In, int[] Ma, int found, 
			LinkedList<Integer> ends_of_occurrence){
		//
		int[] var = new int[] {-1, (In[i] + 1), Ma[i], -1, found};
		//
		return var;
	}
	//
	public static double[] endOfPatterns_MAYBE_a(int i, double[] End, double[] In, double[] Ma, double found, 
			LinkedList<Double> ends_of_occurrence){
		//
		double[] var = new double[] {-1, (In[i] + 1), Ma[i], -1, found};
		//
		return var;
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static int[] endOfPatterns_FOUND(int i, int[] End, int[] In, int[] Ma, int found, 
			LinkedList<Integer> ends_of_occurrence){
		//
		int[] var = new int[] {-1, 1, Ma[i], -1, (i+2)};
		//
		return var;
	}
	//
	public static double[] endOfPatterns_FOUND(int i, double[] End, double[] In, double[] Ma, double found, 
			LinkedList<Double> ends_of_occurrence){
		//
		double[] var = new double[] {-1, 1, Ma[i], -1, (i+2)};
		//
		return var;
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static int[] endOfPatterns_IN(int i, int[] End, int[] In, int[] Ma, int found, 
			LinkedList<Integer> ends_of_occurrence){
		//
		int[] var = new int[] {-1, 1, Ma[i], -1, (i+2)};
		//
		return var;
	}
	//
	public static double[] endOfPatterns_IN(int i, double[] End, double[] In, double[] Ma, double found, 
			LinkedList<Double> ends_of_occurrence){
		//
		double[] var = new double[] {-1, 1, Ma[i], -1, (i+2)};
		//
		return var;
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static int[] endOfPatterns_FOUND_e(int i, int[] End, int[] In, int[] Ma, int found, 
			LinkedList<Integer> ends_of_occurrence){
		//
		ends_of_occurrence.add((i+2));
		int[] var = new int[] {(i+2), In[i+1], Ma[i], Ma[i+1], found};
		//
		return var;
	}
	//
	public static double[] endOfPatterns_FOUND_e(int i, double[] End, double[] In, double[] Ma, double found, 
			LinkedList<Double> ends_of_occurrence){
		//
		ends_of_occurrence.add((double) (i+2));
		double[] var = new double[] {i+2, In[i+1], Ma[i], Ma[i+1], found};
		//
		return var;
	}
	
}

