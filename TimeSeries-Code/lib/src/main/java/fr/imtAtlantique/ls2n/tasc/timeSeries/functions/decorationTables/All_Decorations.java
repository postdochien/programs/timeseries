/*
    The contents of this file are subject to the Mozilla Public License
    Version  1.1  (the "License"); you may not use this file except in
    compliance with the License. You may obtain a copy of the License at:

    http://www.mozilla.org/MPL/

    Software  distributed  under  the License is distributed on an "AS
    IS"  basis,  WITHOUT  WARRANTY  OF  ANY  KIND,  either  express or
    implied.  See  the  License  for  the  specific language governing
    rights and limitations under the License.
    The Original Code is the contents of this file.
    The Developer of the Original Code is IMT Atlantique
    (4 Rue Alfred Kastler, 44300 Nantes, France).
    Contributor(s):
    _____Arnold Hien <arnold.hien@imt-atlantique.fr>
*/

package fr.imtAtlantique.ls2n.tasc.timeSeries.functions.decorationTables;

import java.util.LinkedList;

import fr.imtAtlantique.ls2n.tasc.timeSeries.constants.Constants;
import fr.imtAtlantique.ls2n.tasc.timeSeries.constants.Semantics;
import fr.imtAtlantique.ls2n.tasc.timeSeries.functions.aggreg.Aggregator;
import fr.imtAtlantique.ls2n.tasc.timeSeries.functions.features.Features;
import fr.imtAtlantique.ls2n.tasc.timeSeries.transducers.Transducers;

public class All_Decorations {
	//
	/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	//
	public static double[][] contributions_and_ends(
			double[] X, byte aggreg, byte feature, byte pattern, boolean reverse){
		//
		int n = X.length;
		int m = n-1;
		//
		double found = n+1;
		double[] In = new double[n];
		double[] Ma = new double[n];
		double[][] contribution_and_end = new double[2][n+1]; // [0] --> End, [1] --> contribution
		LinkedList<Double> list_of_ends = new LinkedList<Double>(); // Ends of occurrence
		//
		// Initialisations
		byte after = Constants.get_after_value(pattern);
		byte state = Transducers.STATE_s; // initial state
		byte case_ = Constants.get_case(pattern); // CASE_FOUND OR CASE_FOUND_e
		double C = Aggregator.default_g_f(aggreg, feature, X); // feature value of a found occurrence (found and found_e)
		double D = Features.neutral_f(feature); // feature value of a potential occurrence (maybe_b and maybe_b)
		double R = Aggregator.default_g_f(aggreg, feature, X); // aggregated values of the feature occurrence contribution
		contribution_and_end[0][n] = n + 1; // End[n] = (short) (n+1); --> default value for contribution_and_end[0][n]
		contribution_and_end[0][0] = -1; // End[0] = -1; --> end of pattern
		contribution_and_end[1][0] = R; // R[0] = r; --> contribution
		In[0] = Ma[0] = 0;
		//
		//
		for(int k=0; k<m; k++) {
			//
			byte[] signature_and_semantic = Semantics.get_signature_and_semantic(X[k], X[k+1], pattern, state, reverse);
			byte semantic = signature_and_semantic[1];
			state = signature_and_semantic[2];
			// -----------------------------------------------------------------------------------
			double[] accumulators = Contribution.update_accumulators(k, k+1, X, D, C, R, 
					aggreg, feature, pattern, after, semantic);
			//
			double[] vals = EndOfPatterns.update_endOfPatterns(case_, semantic, k, 
					contribution_and_end[0], In, Ma, found, list_of_ends);
			//
			D = accumulators[0];
			C = accumulators[1];
			R = accumulators[2];
			//
			contribution_and_end[0][k+1] = vals[0];
			In[k+1] = vals[1];
			Ma[k] = vals[2];
			Ma[k+1] = vals[3];
			found = vals[4];
			//
			contribution_and_end[1][k+1] = Aggregator.select_aggregator(aggreg, R, C); // 28/02/2023
		}
		//
		//--------------------------------------------------
		//
		contribution_and_end[1][n] = Aggregator.select_aggregator(aggreg, R, C); 
		//
		// *********************************************************************************
		//
		if(found != n+1) {
			list_of_ends.add(found);
			found = n+1;
		}
		if(In[n-1] <= 1) // the last semantic letter is not a MAYBE_a
			contribution_and_end[0][n-1] = Ma[n-1] = n+1 - In[n-1]; // End[n-1] = Ma[n-1] = n+1 - In[n-1];
		
		list_of_ends.add(contribution_and_end[0][n]); // ends_of_occurrence.add(End[n]);
		//
		// *********************************************************************************
		//
		// second loop to set all contribution_and_end[0] (End) values previously set to -1
		double next_end = list_of_ends.pop();
		for(int k=0; k<n; k++) {
			if(k < next_end) {
				contribution_and_end[0][k] = next_end; // End[k] = next_end;
			}
			if( (k+1) == next_end ) {
				next_end = list_of_ends.pop();
			}
		}
		
		return contribution_and_end;
	}
	//
	/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	//
	public static double[][] contributions_and_ends(
			double[] X, double delta_X, byte aggreg, 
			byte feature, byte pattern, boolean reverse){
		//
		int n = X.length;
		int m = n-1;
		//
		double found = n+1;
		double[] In = new double[n];
		double[] Ma = new double[n];
		double[][] contribution_and_end = new double[2][n+1]; // [0] --> End, [1] --> contribution
		LinkedList<Double> list_of_ends = new LinkedList<Double>(); // Ends of occurrence
		//
		// Initialisations
		byte after = Constants.get_after_value(pattern);
		byte state = Transducers.STATE_s; // initial state
		byte case_ = Constants.get_case(pattern); // CASE_FOUND OR CASE_FOUND_e
		double C = Aggregator.default_g_f(aggreg, feature, X); // feature value of a found occurrence (found and found_e)
		double D = Features.neutral_f(feature); // feature value of a potential occurrence (maybe_b and maybe_b)
		double R = Aggregator.default_g_f(aggreg, feature, X); // aggregated values of the feature occurrence contribution
		contribution_and_end[0][n] = n + 1; // End[n] = (short) (n+1); --> default value for contribution_and_end[0][n]
		contribution_and_end[0][0] = -1; // End[0] = -1; --> end of pattern
		contribution_and_end[1][0] = R; // R[0] = r; --> contribution
		In[0] = Ma[0] = 0;
		//
		//
		for(int k=0; k<m; k++) {
			//
			byte[] signature_and_semantic = Semantics.get_signature_and_semantic(
					X[k], X[k+1], delta_X, pattern, state, reverse);
			byte semantic = signature_and_semantic[1];
			state = signature_and_semantic[2];
			// -----------------------------------------------------------------------------------
			double[] accumulators = Contribution.update_accumulators(k, k+1, X, D, C, R, 
					aggreg, feature, pattern, after, semantic);
			//
			double[] vals = EndOfPatterns.update_endOfPatterns(case_, semantic, k, 
					contribution_and_end[0], In, Ma, found, list_of_ends);
			//
			D = accumulators[0];
			C = accumulators[1];
			R = accumulators[2];
			//
			contribution_and_end[0][k+1] = vals[0];
			In[k+1] = vals[1];
			Ma[k] = vals[2];
			Ma[k+1] = vals[3];
			found = vals[4];
			//
			contribution_and_end[1][k+1] = Aggregator.select_aggregator(aggreg, R, C); // 28/02/2023
		}
		//
		//--------------------------------------------------
		//
		contribution_and_end[1][n] = Aggregator.select_aggregator(aggreg, R, C); 
		//
		// *********************************************************************************
		//
		if(found != n+1) {
			list_of_ends.add(found);
			found = n+1;
		}
		if(In[n-1] <= 1) // the last semantic letter is not a MAYBE_a
			contribution_and_end[0][n-1] = Ma[n-1] = n+1 - In[n-1]; // End[n-1] = Ma[n-1] = n+1 - In[n-1];
		
		list_of_ends.add(contribution_and_end[0][n]); // ends_of_occurrence.add(End[n]);
		//
		// *********************************************************************************
		//
		// second loop to set all contribution_and_end[0] (End) values previously set to -1
		double next_end = list_of_ends.pop();
		for(int k=0; k<n; k++) {
			if(k < next_end) {
				contribution_and_end[0][k] = next_end; // End[k] = next_end;
			}
			if( (k+1) == next_end ) {
				next_end = list_of_ends.pop();
			}
		}
		//
		return contribution_and_end;
	}
	//
	/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	//
	public static double[][] contributions_and_ends_BinMultivariate(
			double[][] X, double delta_X, byte aggreg, 
			byte feature, byte pattern, boolean reverse){
		//
		int n = X[0].length;
		int m = n-1;
		//
		double found = n+1;
		double[] In = new double[n];
		double[] Ma = new double[n];
		double[][] contribution_and_end = new double[2][n+1]; // [0] --> End, [1] --> contribution
		LinkedList<Double> list_of_ends = new LinkedList<Double>(); // Ends of occurrence
		//
		// Initialisations
		byte after = Constants.get_after_value(pattern);
		byte state = Transducers.STATE_s; // initial state
		byte case_ = Constants.get_case(pattern); // CASE_FOUND OR CASE_FOUND_e
		double C = Aggregator.default_g_f(aggreg, feature, X[0]); // feature value of a found occurrence (found and found_e)
		double D = Features.neutral_f(feature); // feature value of a potential occurrence (maybe_b and maybe_b)
		double R = Aggregator.default_g_f(aggreg, feature, X[0]); // aggregated values of the feature occurrence contribution
		contribution_and_end[0][n] = n + 1; // End[n] = (short) (n+1); --> default value for contribution_and_end[0][n]
		contribution_and_end[0][0] = -1; // End[0] = -1; --> end of pattern
		contribution_and_end[1][0] = R; // R[0] = r; --> contribution
		In[0] = Ma[0] = 0;
		//
		//
		for(int k=0; k<m; k++) {
			//
			byte[] signature_and_semantic = Semantics.get_BinMultivariate_signature_and_semantic(
					X, k, k+1, delta_X, pattern, state, reverse);
			byte semantic = signature_and_semantic[1];
			state = signature_and_semantic[2];
			// -----------------------------------------------------------------------------------
			double[] accumulators = Contribution.update_accumulators(k, k+1, X[0], D, C, R, 
					aggreg, feature, pattern, after, semantic);
			//
			double[] vals = EndOfPatterns.update_endOfPatterns(case_, semantic, k, 
					contribution_and_end[0], In, Ma, found, list_of_ends);
			//
			D = accumulators[0];
			C = accumulators[1];
			R = accumulators[2];
			//
			contribution_and_end[0][k+1] = vals[0];
			In[k+1] = vals[1];
			Ma[k] = vals[2];
			Ma[k+1] = vals[3];
			found = vals[4];
			//
			contribution_and_end[1][k+1] = Aggregator.select_aggregator(aggreg, R, C); // 28/02/2023
		}
		//
		//--------------------------------------------------
		//
		contribution_and_end[1][n] = Aggregator.select_aggregator(aggreg, R, C); 
		//
		// *********************************************************************************
		//
		if(found != n+1) {
			list_of_ends.add(found);
			found = n+1;
		}
		if(In[n-1] <= 1) // the last semantic letter is not a MAYBE_a
			contribution_and_end[0][n-1] = Ma[n-1] = n+1 - In[n-1]; // End[n-1] = Ma[n-1] = n+1 - In[n-1];
		
		list_of_ends.add(contribution_and_end[0][n]); // ends_of_occurrence.add(End[n]);
		//
		// *********************************************************************************
		//
		// second loop to set all contribution_and_end[0] (End) values previously set to -1
		double next_end = list_of_ends.pop();
		for(int k=0; k<n; k++) {
			if(k < next_end) {
				contribution_and_end[0][k] = next_end; // End[k] = next_end;
			}
			if( (k+1) == next_end ) {
				next_end = list_of_ends.pop();
			}
		}
		//
		return contribution_and_end;
	}
	//
	/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	//
}
