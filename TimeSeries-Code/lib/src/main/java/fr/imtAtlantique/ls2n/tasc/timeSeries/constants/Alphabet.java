/*
    The contents of this file are subject to the Mozilla Public License
    Version  1.1  (the "License"); you may not use this file except in
    compliance with the License. You may obtain a copy of the License at:

    http://www.mozilla.org/MPL/

    Software  distributed  under  the License is distributed on an "AS
    IS"  basis,  WITHOUT  WARRANTY  OF  ANY  KIND,  either  express or
    implied.  See  the  License  for  the  specific language governing
    rights and limitations under the License.
    The Original Code is the contents of this file.
    The Developer of the Original Code is IMT Atlantique
    (4 Rue Alfred Kastler, 44300 Nantes, France).
    Contributor(s):
    _____Arnold Hien <arnold.hien@imt-atlantique.fr>
*/

package fr.imtAtlantique.ls2n.tasc.timeSeries.constants;

public class Alphabet {
	/*
	 * #-----------------------------------------------------------------#
	 * |                     Input Alphabet Letters                      |
	 * #-----------------------------------------------------------------#
	 */
	public static final byte EQUAL = 0; // letter '='
	public static final byte LESSER = 1; // letter '<'
	public static final byte GREATER = 2; // letter '>'
	public static final byte LESSER_EQUAL = 3; // letter '<='
	public static final byte GREATER_EQUAL = 4; // letter '>='
	public static final byte OTHER = 10; // letter '???'
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	/*
	public static byte get_SIGNATURE_class(byte[] sign) {
		int[] occurrences_of_each_sign = occurrencesOfEachSign(sign);
		byte sign_ = (occurrences_of_each_sign[0] > 0) ? LESSER : 
			((occurrences_of_each_sign[1] > 0) ? EQUAL : GREATER);
		return sign_;
	}
	*/
	public static byte getSignature(double x_1, double x_2) {
		return x_1 < x_2 ? LESSER : (x_1 == x_2 ? EQUAL : GREATER );
	}
	//
	public static byte getSignature(double x_1, double x_2, double deltaVal) {
		return ((x_2-x_1) > deltaVal) ? LESSER : (((x_1-x_2) > deltaVal) ? GREATER : EQUAL );
	}
	//
	public static byte getBinMultivariateSignature(double X[][], int k1, int k2, double deltaVal) {
		byte s1 = ((X[0][k2]-X[0][k1]) > deltaVal) ? LESSER : (((X[0][k1]-X[0][k2]) > deltaVal) ? GREATER : EQUAL);
		byte s2 = ((X[1][k2]-X[1][k1]) > deltaVal) ? LESSER : (((X[1][k1]-X[1][k2]) > deltaVal) ? GREATER : EQUAL);
		//------------------------------------------
		if( (s1== EQUAL) && (s2==EQUAL) )
			return EQUAL;
		else if( (s1== LESSER) && (s2==LESSER) )
			return LESSER;
		else if( (s1== GREATER) && (s2==GREATER) )
			return GREATER;
		else if( ((s1== LESSER) && (s2==EQUAL)) || ((s2== LESSER) && (s1==EQUAL)) )
			return LESSER_EQUAL;
		else if( ((s1== GREATER) && (s2==EQUAL)) || ((s2== GREATER) && (s1==EQUAL)) )
			return GREATER_EQUAL;
		return OTHER;
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static int[] occurrencesOfEachSign(byte[] sign) {
		int[] occurrences_of_each_sign = new int[] { 0, 0, 0 }; // 0 --> '<', 1 --> '=', 2 --> '>'
		int nbMeasures = sign.length;
		for(int k=0; k<nbMeasures; k++)
			occurrences_of_each_sign[sign[k] + 1]++;
		return occurrences_of_each_sign;
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static byte get_SIGNATURE_class(byte[] sign) { // 0 --> '<', 1 --> '=', 2 --> '>'
		int[] occurrences_of_each_sign = occurrencesOfEachSign(sign);
		byte sign_ = OTHER;
		//byte sign_ = (occurrences_of_each_sign[0] > 0) ? LESSER : 
		//	((occurrences_of_each_sign[1] > 0) ? EQUAL : GREATER);
		if((occurrences_of_each_sign[0] > 0) && (occurrences_of_each_sign[1] == 0) && 
				(occurrences_of_each_sign[2] == 0))
			sign_ = LESSER;
		else if ((occurrences_of_each_sign[0] == 0) && (occurrences_of_each_sign[1] > 0) && 
				(occurrences_of_each_sign[2] == 0))
			sign_ = EQUAL;
		else if ((occurrences_of_each_sign[0] == 0) && (occurrences_of_each_sign[1] == 0) && 
				(occurrences_of_each_sign[2] > 0))
			sign_ = GREATER;
		return sign_;
	}
	/*
	 * #-----------------------------------------------------------------#
	 * |                Signature between X[i] AND X[i+1]                |
	 * #-----------------------------------------------------------------#
	 */
	public static byte get_one_Signature(double[][] seq, int i1, int i2) {
		int nbMeasures = seq.length;
		//
		byte S = Byte.MIN_VALUE;
		byte[] sign = new byte[nbMeasures];
		for(int i=0; i<nbMeasures; i++) 
			sign[i] = getSignature(seq[i][i1], seq[i][i2]);
		S = get_SIGNATURE_class(sign);
		//
		return S;
	}
	//
	public static byte get_one_Signature(double[][] seq, int i1, int i2, double deltaVal) {
		int nbMeasures = seq.length;
		//
		byte S = Byte.MIN_VALUE;
		byte[] sign = new byte[nbMeasures];
		for(int i=0; i<nbMeasures; i++) 
			sign[i] = getSignature(seq[i][i1], seq[i][i2], deltaVal);
		S = get_SIGNATURE_class(sign);
		//, double deltaVal
		return S;
	}
	/*
	 * #-----------------------------------------------------------------#
	 * |                  Get all consecutive signatures                 |
	 * #-----------------------------------------------------------------#
	 */
	public static byte[] get_all_Signature(double[] seq) {
		int n = seq.length;
		byte[] S = new byte[n-1];
		for(int i=0; i<n-1; i++) {
			S[i] = getSignature(seq[i], seq[i+1]);
		}
		return S;
	}
	//
	public static byte[] get_all_Signature(double[] seq, double deltaVal) {
		int n = seq.length;
		byte[] S = new byte[n-1];
		for(int i=0; i<n-1; i++) {
			S[i] = getSignature(seq[i], seq[i+1], deltaVal);
		}
		return S;
	}
	//
	public static byte[][] get_all_Signature(double[][] seq) {
		int nbMeasures = seq.length;
		byte[][] S = new byte[nbMeasures][];
		for(int i=0; i<nbMeasures; i++) {
			S[i] = get_all_Signature(seq[i]);
		}
		return S;
	}
	//
	public static byte[][] get_all_Signature(double[][] seq, double deltaVal) {
		int nbMeasures = seq.length;
		byte[][] S = new byte[nbMeasures][];
		for(int i=0; i<nbMeasures; i++) {
			S[i] = get_all_Signature(seq[i], deltaVal);
		}
		return S;
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static byte[] get_all_Signature_(double[][] seq) {
		//int nbMeasures = seq.length;
		int n = seq[0].length;
		byte[] S = new byte[n-1];
		
		for(int j=0; j<n-1; j++) {
			//byte[] sign = new byte[nbMeasures];
			//for(int i=0; i<nbMeasures; i++) {
			//	sign[i] = getSignature(seq[i][j], seq[i][j+1]);
			//}
			S[j] = get_one_Signature(seq, j, j+1);
		}
		return S;
	}
	//
	public static byte[] get_all_Signature_(double[][] seq, double deltaVal) {
		//int nbMeasures = seq.length;
		int n = seq[0].length;
		byte[] S = new byte[n-1];
		
		for(int j=0; j<n-1; j++) {
			S[j] = get_one_Signature(seq, j, j+1, deltaVal);
		}
		return S;
	}
	/*
	 * #-----------------------------------------------------------------#
	 * |                     Get reverse signatures                      |
	 * #-----------------------------------------------------------------#
	 */
	public static byte[] reverse_sign(byte[] sign) {
		int m = sign.length;
		byte[] rev = new byte[m];
		for(int i = 0; i < m; i++) {
			if(sign[i] == LESSER)
				rev[i] = GREATER;
			else if(sign[i] == GREATER)
				rev[i] = LESSER;
			else
				rev[i] = sign[i];
		}
		return rev;
	}
	/*
	 * #-----------------------------------------------------------------#
	 * |                     Get signatures as STRING                    |
	 * #-----------------------------------------------------------------#
	 */
	public static char getSignature_STR(byte sign) {
		return (sign==LESSER) ? '<' : ((sign==GREATER) ? '>' : '=');
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static String getSignature_STR_(byte sign) {
		if(sign==EQUAL)
			return "=";
		else if(sign==LESSER)
			return "<";
		else if(sign==GREATER)
			return ">";
		else if(sign==LESSER_EQUAL)
			return "<=";
		else if(sign==GREATER_EQUAL)
			return ">=";
		return "@";
		//return (sign==LESSER) ? '<' : ((sign==GREATER) ? '>' : '=');
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static char[] getSignature_STR(byte[] sign) {
		int n = sign.length;
		char[] sign_str = new char[n];
		for(int i = 0; i < n; i++) {
			sign_str[i] = getSignature_STR(sign[i]);
		}
		return sign_str;
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	
	
	
	
}
