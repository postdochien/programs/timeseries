/*
    The contents of this file are subject to the Mozilla Public License
    Version  1.1  (the "License"); you may not use this file except in
    compliance with the License. You may obtain a copy of the License at:

    http://www.mozilla.org/MPL/

    Software  distributed  under  the License is distributed on an "AS
    IS"  basis,  WITHOUT  WARRANTY  OF  ANY  KIND,  either  express or
    implied.  See  the  License  for  the  specific language governing
    rights and limitations under the License.
    The Original Code is the contents of this file.
    The Developer of the Original Code is IMT Atlantique
    (4 Rue Alfred Kastler, 44300 Nantes, France).
    Contributor(s):
    _____Arnold Hien <arnold.hien@imt-atlantique.fr>
*/

package fr.imtAtlantique.ls2n.tasc.timeSeries.functions.features;

public class Max extends Features {
	
	public static double neutral_f = Float.NEGATIVE_INFINITY;
	public static double min_f = Float.NEGATIVE_INFINITY;
	public static double max_f = Float.POSITIVE_INFINITY;
	
	public static double neutral_f() {
		return Float.NEGATIVE_INFINITY;
	}
	
	public static double min_f(double ... vals) {
		return Float.NEGATIVE_INFINITY;
	}
	
	public static double max_f(double ... vals) {
		return Float.POSITIVE_INFINITY;
	}
	
	public static double phi_f(double ... vals) {
		return Features.max_tab(vals);
	}
	
	public static double delta_f(double ... vals) {
		double X_i = vals[0];
		return X_i;
	}
	
	public static Class<Max> get_features() {
		return Max.class;
	}
	
}
