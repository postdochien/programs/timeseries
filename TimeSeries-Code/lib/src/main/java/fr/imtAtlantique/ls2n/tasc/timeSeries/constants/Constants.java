/*
    The contents of this file are subject to the Mozilla Public License
    Version  1.1  (the "License"); you may not use this file except in
    compliance with the License. You may obtain a copy of the License at:

    http://www.mozilla.org/MPL/

    Software  distributed  under  the License is distributed on an "AS
    IS"  basis,  WITHOUT  WARRANTY  OF  ANY  KIND,  either  express or
    implied.  See  the  License  for  the  specific language governing
    rights and limitations under the License.
    The Original Code is the contents of this file.
    The Developer of the Original Code is IMT Atlantique
    (4 Rue Alfred Kastler, 44300 Nantes, France).
    Contributor(s):
    _____Arnold Hien <arnold.hien@imt-atlantique.fr>
*/

package fr.imtAtlantique.ls2n.tasc.timeSeries.constants;

public final class Constants {
	/*
	 * #-----------------------------------------------------------------#
	 * |               Case of Ends of patterns' Transducer              |
	 * #-----------------------------------------------------------------#
	 */
	public static final byte CASE_FOUND = 1; // 
	public static final byte CASE_FOUND_e = 2; // 
	/*
	 * #-----------------------------------------------------------------#
	 * |                           All Patterns                          |
	 * #-----------------------------------------------------------------#
	 */
	public static final byte Inflexion = 1;
	/* -------------------------------------------- */
	public static final byte BumpOnDecSeq = 2;
	public static final byte DipOnIncSeq = 3;
	public static final byte Dec = 4;
	public static final byte Inc = 5;
	public static final byte Steady = 6;
	/* -------------------------------------------- */
	public static final byte DecTerrace = 7;
	public static final byte IncTerrace = 8;
	/* -------------------------------------------- */
	public static final byte Plain = 9;
	public static final byte Plateau = 10;
	public static final byte ProperPlain = 11;
	public static final byte ProperPlateau = 12;
	/* -------------------------------------------- */
	public static final byte Gorge = 13;
	public static final byte Summit = 14;
	public static final byte Peak = 15;
	public static final byte Valley = 16;
	/* -------------------------------------------- */
	public static final byte DecSeq = 17;
	public static final byte IncSeq = 18;
	public static final byte SteadySeq = 19;
	public static final byte StrictlyDecSeq = 20;
	public static final byte StrictlyIncSeq = 21;
	/* -------------------------------------------- */
	public static final byte Zigzag = 22;
	/* -------------------------------------------- */
	public static final byte TempHumidDec = 23;
	public static final byte TempHumidInc = 24;
	/*
	 * #-----------------------------------------------------------------#
	 * |                   TWO CATEGORIES OF PATTERNS                    |
	 * #-----------------------------------------------------------------#
	 */
	public static byte get_case(byte pattern) {
		switch(pattern){
			case Constants.BumpOnDecSeq:
			case Constants.Dec:
			case Constants.DecTerrace:
			case Constants.DipOnIncSeq:
			case Constants.Inc:
			case Constants.IncTerrace:
			case Constants.Inflexion:
			case Constants.Plain:
			case Constants.Plateau:
			case Constants.ProperPlain:
			case Constants.ProperPlateau:
			case Constants.Steady:
				return Constants.CASE_FOUND_e;
			case Constants.DecSeq:
			case Constants.Gorge:
			case Constants.IncSeq:
			case Constants.Peak:
			case Constants.SteadySeq:
			case Constants.StrictlyDecSeq:
			case Constants.StrictlyIncSeq:
			case Constants.Summit:
			case Constants.Valley:
			case Constants.Zigzag:
				return Constants.CASE_FOUND;
			default:
				return Constants.CASE_FOUND;
		}
	}
	/*
	 * #-----------------------------------------------------------------#
	 * |                   TWO CATEGORIES OF PATTERNS                    |
	 * #-----------------------------------------------------------------#
	 */
	public static byte get_after_value(byte pattern) {
		switch(pattern){
			case Constants.BumpOnDecSeq:
			case Constants.DecTerrace:
			case Constants.DipOnIncSeq:
			case Constants.Gorge:
			case Constants.IncTerrace:
			case Constants.Inflexion:
			case Constants.Peak:
			case Constants.Plain:
			case Constants.Plateau:
			case Constants.ProperPlain:
			case Constants.ProperPlateau:
			case Constants.Summit:
			case Constants.Valley:
			case Constants.Zigzag:
				return (byte) 1;
			case Constants.Dec:
			case Constants.DecSeq:
			case Constants.Inc:
			case Constants.IncSeq:
			case Constants.Steady:
			case Constants.SteadySeq:
			case Constants.StrictlyDecSeq:
			case Constants.StrictlyIncSeq:
				return (byte) 0;
			default:
				return (byte) 0;
		}
	}
	/*
	 * #-----------------------------------------------------------------#
	 * |                                                                 |
	 * #-----------------------------------------------------------------#
	 */
	
	/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

	
}
