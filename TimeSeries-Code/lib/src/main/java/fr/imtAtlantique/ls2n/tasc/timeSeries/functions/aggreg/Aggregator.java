/*
    The contents of this file are subject to the Mozilla Public License
    Version  1.1  (the "License"); you may not use this file except in
    compliance with the License. You may obtain a copy of the License at:

    http://www.mozilla.org/MPL/

    Software  distributed  under  the License is distributed on an "AS
    IS"  basis,  WITHOUT  WARRANTY  OF  ANY  KIND,  either  express or
    implied.  See  the  License  for  the  specific language governing
    rights and limitations under the License.
    The Original Code is the contents of this file.
    The Developer of the Original Code is IMT Atlantique
    (4 Rue Alfred Kastler, 44300 Nantes, France).
    Contributor(s):
    _____Arnold Hien <arnold.hien@imt-atlantique.fr>
*/

package fr.imtAtlantique.ls2n.tasc.timeSeries.functions.aggreg;

import fr.imtAtlantique.ls2n.tasc.timeSeries.functions.features.Features;
import fr.imtAtlantique.ls2n.tasc.timeSeries.functions.features.Max;
import fr.imtAtlantique.ls2n.tasc.timeSeries.functions.features.Min;
import fr.imtAtlantique.ls2n.tasc.timeSeries.functions.features.One;
import fr.imtAtlantique.ls2n.tasc.timeSeries.functions.features.Range;
import fr.imtAtlantique.ls2n.tasc.timeSeries.functions.features.Surface;
import fr.imtAtlantique.ls2n.tasc.timeSeries.functions.features.Width;

public class Aggregator {
	/*
	 * #-----------------------------------------------------------------#
	 * |                          All Aggregators                        |
	 * #-----------------------------------------------------------------#
	 */
	public static final byte Max_g = 1;
	public static final byte Min_g = 2;
	public static final byte Sum_g = 3;
	/*
	 * #-----------------------------------------------------------------#
	 * |                     REVERSE THE AGGREGATOR                      |
	 * #-----------------------------------------------------------------#
	 */
	public static byte reverse_aggreg(byte aggreg) {
		switch(aggreg){
			case Sum_g:
				return Sum_g;
			case Max_g:
				return Min_g;
			case Min_g:
				return Max_g;
			default:
				return Sum_g;
		}
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static double select_aggregator(byte aggreg, double ... vals) {
		switch(aggreg) {
			case Max_g:
				return max_g(vals);
			case Min_g:
				return min_g(vals);
			case Sum_g:
				return sum_g(vals);
			default:
				return sum_g(vals);
		}
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static double min_g(double ... vals) {
		return Features.min_tab(vals);
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static double max_g(double ... vals) {
		return Features.max_tab(vals);
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static double sum_g(double ... vals) {
		return Features.sum_tab(vals);
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static double default_g_f(byte aggreg, byte features, double ... vals) {
		switch(aggreg) {
			case Max_g:
				switch(features) {
					case Features.Max_f:
						return Max.min_f(vals);
					case Features.Min_f:
						return Min.min_f(vals);
					case Features.One_f:
						return One.min_f(vals);
					case Features.Range_f:
						return Range.min_f(vals);
					case Features.Surface_f:
						return Surface.min_f(vals);
					case Features.Width_f:
						return Width.min_f(vals);
				}
			case Min_g:
				switch(features) {
					case Features.Max_f:
						return Max.max_f(vals);
					case Features.Min_f:
						return Min.max_f(vals);
					case Features.One_f:
						return One.max_f(vals);
					case Features.Range_f:
						return Range.max_f(vals);
					case Features.Surface_f:
						return Surface.max_f(vals);
					case Features.Width_f:
						return Width.max_f(vals);
				}
			case Sum_g:
				return 0;
			default:
				return -1;
		}
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static double phi_g(byte aggreg, double ... vals) {
		switch(aggreg) {
			case Sum_g:
				return sum_g(vals);
			case Max_g:
				return max_g(vals);
			case Min_g:
				return min_g(vals);
			default:
				return sum_g(vals);
		}
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
}
