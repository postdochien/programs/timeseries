/*
    The contents of this file are subject to the Mozilla Public License
    Version  1.1  (the "License"); you may not use this file except in
    compliance with the License. You may obtain a copy of the License at:

    http://www.mozilla.org/MPL/

    Software  distributed  under  the License is distributed on an "AS
    IS"  basis,  WITHOUT  WARRANTY  OF  ANY  KIND,  either  express or
    implied.  See  the  License  for  the  specific language governing
    rights and limitations under the License.
    The Original Code is the contents of this file.
    The Developer of the Original Code is IMT Atlantique
    (4 Rue Alfred Kastler, 44300 Nantes, France).
    Contributor(s):
    _____Arnold Hien <arnold.hien@imt-atlantique.fr>
*/

package fr.imtAtlantique.ls2n.tasc.timeSeries.constants;

import fr.imtAtlantique.ls2n.tasc.timeSeries.transducers.Transducers;

public class Semantics {
	/*
	 * #-----------------------------------------------------------------#
	 * |           Output Alphabet Letters (semantic letters)            |
	 * #-----------------------------------------------------------------#
	 */
	public static final byte OUT = 1; // out
	public static final byte MAYBE_b = 2; // maybe before
	public static final byte OUT_r = 3; // out reset
	public static final byte FOUND = 4; // found
	public static final byte MAYBE_a = 5; // maybe after
	public static final byte IN = 6; // in
	public static final byte OUT_a = 7; // out after
	public static final byte FOUND_e = 8; // found end
	/* 
	 * # ---------------------------------------------------------------------------------- #
	 * |                       get the signature and semantic letters                       |
	 * # ---------------------------------------------------------------------------------- #
	 */
	public static byte[] get_signature_and_semantic(byte sign, byte pattern, byte state, boolean reverse) {
		//
		byte[] sign_and_semantic = new byte[3];
		sign_and_semantic[0] = sign; // get the signature
		//
		// get all transitions : states and semantic letters from the transducer
		byte[] out = reverse ? 
				Transducers.selectReverseTransducer(pattern, state, sign_and_semantic[0]) :
				Transducers.selectTransducer(pattern, state, sign_and_semantic[0]);
		sign_and_semantic[1] = out[1]; // semantic letter
		sign_and_semantic[2] = out[0]; // state
		//
		return sign_and_semantic;
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static byte[] get_signature_and_semantic(
			double x1, double x2, byte pattern, byte state, boolean reverse) {
		//
		byte[] sign_and_semantic = new byte[3];
		sign_and_semantic[0] = Alphabet.getSignature(x1, x2); // get the signature
		//
		// get all transitions : states and semantic letters from the transducer
		byte[] out = reverse ? 
				Transducers.selectReverseTransducer(pattern, state, sign_and_semantic[0]) :
				Transducers.selectTransducer(pattern, state, sign_and_semantic[0]);
		sign_and_semantic[1] = out[1]; // semantic letter
		sign_and_semantic[2] = out[0]; // state
		//
		return sign_and_semantic;
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static byte[] get_signature_and_semantic(
			double x1, double x2, double delta_X, byte pattern, byte state, boolean reverse) {
		//
		byte[] sign_and_semantic = new byte[3];
		sign_and_semantic[0] = Alphabet.getSignature(x1, x2, delta_X); // get the signature
		//
		// get all transitions : states and semantic letters from the transducer
		byte[] out = reverse ? 
				Transducers.selectReverseTransducer(pattern, state, sign_and_semantic[0]) :
				Transducers.selectTransducer(pattern, state, sign_and_semantic[0]);
		sign_and_semantic[1] = out[1]; // semantic letter
		sign_and_semantic[2] = out[0]; // state
		//
		return sign_and_semantic;
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static byte[] get_BinMultivariate_signature_and_semantic(
			double X[][], int k1, int k2, double delta_seq, byte pattern, byte state, boolean reverse) {
		//
		byte[] sign_and_semantic = new byte[3];
		sign_and_semantic[0] = Alphabet.getBinMultivariateSignature(X, k1, k2, delta_seq); // get the signature
		//
		// get all transitions : states and semantic letters from the transducer
		byte[] out = reverse ? 
				Transducers.selectReverseTransducer(pattern, state, sign_and_semantic[0]) :
				Transducers.selectTransducer(pattern, state, sign_and_semantic[0]);
		sign_and_semantic[1] = out[1]; // semantic letter
		sign_and_semantic[2] = out[0]; // state
		//
		return sign_and_semantic;
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static byte[][] get_signature_and_semantic(double[] seq, byte pattern, boolean reverse) {
		//
		int n = seq.length; // length of the sequence
		byte currentState = Transducers.initial_state;
		byte[][] sign_and_semantic = new byte[2][n-1];
		//
		for(int i=0; i<n-1; i++) {
			byte[] out = get_signature_and_semantic(Alphabet.getSignature(seq[i], seq[i+1]), pattern, currentState, reverse);
			sign_and_semantic[0][i] = out[0]; // signature
			sign_and_semantic[1][i] = out[1]; // semantic letter
			currentState = out[2]; // new state
			
		}
		return sign_and_semantic;
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static byte[][] get_signature_and_semantic(double[] seq, double delta_X, byte pattern, boolean reverse) {
		//
		int n = seq.length; // length of the sequence
		byte currentState = Transducers.initial_state;
		byte[][] sign_and_semantic = new byte[2][n-1];
		//
		for(int i=0; i<n-1; i++) {
			byte[] out = get_signature_and_semantic(
					Alphabet.getSignature(seq[i], seq[i+1], delta_X), pattern, currentState, reverse);
			sign_and_semantic[0][i] = out[0]; // signature
			sign_and_semantic[1][i] = out[1]; // semantic letter
			currentState = out[2]; // new state
			
		}
		return sign_and_semantic;
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static byte[][][] get_signature_and_semantic_per_measures(double[][] seq, byte pattern, boolean reverse) {
		//
		int nbMeasures = seq.length; // nb of measures
		int n = seq[0].length; // length of the sequence
		//
		byte[][][] sign_and_semantic = new byte[nbMeasures][2][n-1];
		for(int i=0; i<nbMeasures; i++) {
			sign_and_semantic[i] = get_signature_and_semantic(seq[i], pattern, reverse);
			
		}
		return sign_and_semantic;
	}
	/* 
	 * # ---------------------------------------------------------------------------------- #
	 * |                   get the string signature and semantic letters                    |
	 * # ---------------------------------------------------------------------------------- #
	 */
	public static char[][][] get_signature_and_semantic_STR(double[] seq, byte pattern, boolean reverse) {
		int n = seq.length; // length of the sequence
		byte currentState = Transducers.initial_state;
		char[][][] sign_and_semantic = new char[2][n-1][2]; // [sign or semantic][n-1]
		//
		for(int i=0; i<n-1; i++) {
			//
			byte[] out = get_signature_and_semantic(Alphabet.getSignature(seq[i], seq[i+1]), pattern, currentState, reverse);
			sign_and_semantic[0][i][0] = Alphabet.getSignature_STR(out[0]); // signature
			sign_and_semantic[1][i] = get_semantic_STR(out[1]); // semantic letter
			currentState = out[2]; // new state
			//
		}
		return sign_and_semantic;
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static char[][][] get_signature_and_semantic_STR(double[][] seq, byte pattern, boolean reverse) {
		int n = seq[0].length; // length of the sequence
		
		byte currentState = Transducers.initial_state;
		char[][][] sign_and_semantic = new char[2][n-1][2]; // [sign or semantic][n-1]
		//
		for(int i=0; i<n-1; i++) {
			//
			byte[] out = get_signature_and_semantic(Alphabet.get_one_Signature(seq, i, i+1), pattern, currentState, reverse);
			sign_and_semantic[0][i][0] = Alphabet.getSignature_STR(out[0]); // signature
			sign_and_semantic[1][i] = get_semantic_STR(out[1]); // semantic letter
			currentState = out[2]; // new state
			//
		}
		return sign_and_semantic;
	}
	/* 
	 * # ---------------------------------------------------------------------------------- #
	 * |                    get a string semantic letter from a sequence                    |
	 * # ---------------------------------------------------------------------------------- #
	 */
	public static char[] get_semantic_STR(byte semantic) {
		char[] semantic_STR; // = new char[2];
		switch(semantic) {
			case OUT:
				semantic_STR = new char[]{'O', '.'};
				break;
			case MAYBE_b:
				semantic_STR = new char[]{'M', 'b'};
				break;
			case OUT_r:
				semantic_STR = new char[]{'O', 'r'};
				break;
			case FOUND:
				semantic_STR = new char[]{'F', '.'};
				break;
			case FOUND_e:
				semantic_STR = new char[]{'F', 'e'};
				break;
			case IN:
				semantic_STR = new char[]{'I', '.'};
				break;
			case MAYBE_a:
				semantic_STR = new char[]{'M', 'a'};
				break;
			case OUT_a:
				semantic_STR = new char[]{'O', 'a'};
				break;
			default:
				semantic_STR = new char[]{'O', '.'};
				break;
		}
		return semantic_STR;
	}
	/*
	 * ----------------------------------------------------------------------------------------------
	 */
	public static char[][] get_semantic_STR(byte[] semantic) {
		
		int n = semantic.length;
		char[][] semantic_ = new char[n][2];
		for(int i = 0; i < n; i++) {
			semantic_[i] = get_semantic_STR(semantic[i]);
		}
		//
		return semantic_;
	}
	/* 
	 * # ---------------------------------------------------------------------------------- #
	 * |                                                                                    |
	 * # ---------------------------------------------------------------------------------- #
	 */
	
}
