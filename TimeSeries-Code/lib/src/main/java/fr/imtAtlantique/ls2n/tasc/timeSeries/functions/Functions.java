/*
    The contents of this file are subject to the Mozilla Public License
    Version  1.1  (the "License"); you may not use this file except in
    compliance with the License. You may obtain a copy of the License at:

    http://www.mozilla.org/MPL/

    Software  distributed  under  the License is distributed on an "AS
    IS"  basis,  WITHOUT  WARRANTY  OF  ANY  KIND,  either  express or
    implied.  See  the  License  for  the  specific language governing
    rights and limitations under the License.
    The Original Code is the contents of this file.
    The Developer of the Original Code is IMT Atlantique
    (4 Rue Alfred Kastler, 44300 Nantes, France).
    Contributor(s):
    _____Arnold Hien <arnold.hien@imt-atlantique.fr>
*/

package fr.imtAtlantique.ls2n.tasc.timeSeries.functions;

//import timeSeries.constants.Alphabet;
import fr.imtAtlantique.ls2n.tasc.timeSeries.functions.decorationTables.All_Decorations;

public class Functions {
	public static int begin = 0;
	public static int end = 0;
	/* 
	 * # ---------------------------------------------------------------------------------- #
	 * |                              get the reverse sequence                              |
	 * # ---------------------------------------------------------------------------------- #
	 */
	public static double[] reverse_seq(double[] X) {
		int n = X.length;
		double[] rev = new double[n];
		for(int i = 0; i < n; i++) {
			rev[i] = X[n-i-1];
		}
		return rev;
	}
	//
	public static double[][] reverse_seq(double[][] X) {
		int nb = X.length;
		int n = X[0].length;
		double[][] rev = new double[nb][n];
		for(int i = 0; i < nb; i++) {
			for(int j = 0; j < n; j++) {
				rev[i][j] = X[i][n-j-1];
			}
		}
		return rev;
	}
	//
	/* 
	 * # ---------------------------------------------------------------------------------- #
	 * |                                                                                    |
	 * # ---------------------------------------------------------------------------------- #
	 */
	public static double window_contribution(
			int i, int j, int n, 
			double[] all_contributions, 
			double[] all_contributions_REV) {
		return all_contributions[j] + all_contributions_REV[i] - all_contributions[n-1];
	}
	//
	public static double window_contribution(
			int i, int j, int n, 
			double[][] all_contributions_and_ends, 
			double[][] all_contributions_and_ends_REV) {
		//
		double contrib = 
				all_contributions_and_ends[1][j] + 
				all_contributions_and_ends_REV[1][i] - 
				all_contributions_and_ends[1][n-1];
		//
		boolean pattern_end_before = Math.min(n, all_contributions_and_ends[0][j]) <= (j+1);
		boolean pattern_start_after = Math.min(n, all_contributions_and_ends[0][i]) <= n;
		if(pattern_end_before && pattern_start_after) {
			return contrib;
		}
		return 0;
		
		//return all_contributions[j] + all_contributions_REV[i] - all_contributions[n-1];
	}
	/* 
	 * # ---------------------------------------------------------------------------------- #
	 * |                                                                                    |
	 * # ---------------------------------------------------------------------------------- #
	 */
	public static void sliding_windows_contribution(
			double[][] X, int windowSize, 
			byte aggreg, byte feature, byte pattern) {
		//
		int nbMeasures = X.length;
		if(nbMeasures < 1)
			return;
		//
		int N = X[0].length;
		//
		//*
		if(end == 0) {
			begin = 0;
			end = N-1;
		}
		else {
			begin = (end+1)-(windowSize-1);
			end = end + N - (windowSize-1);
		}
		//*/
		//
		double[][] X_r = Functions.reverse_seq(X);
		double[][][] contribs_and_ends = new double[nbMeasures][][]; // contributions and ends
		double[][][] contribs_and_ends_REV = new double[nbMeasures][][]; // reverse contributions and ends
		//
		//System.out.println(
		//		"\n\n\n\n*************************************\n*************************************\n\n");
		//
		for(int k=0; k<nbMeasures; k++) {
			//
			//------------------------------
			//
			contribs_and_ends[k] = All_Decorations.contributions_and_ends(
					X[k], aggreg, feature, pattern, false);
			contribs_and_ends_REV[k] = All_Decorations.contributions_and_ends(
					X_r[k], aggreg, feature, pattern, true);
			//
			//------------------------------
			//
			// ***************************************************************
			//
			System.out.println("Measure #" + (k+1) + " : ");
			System.out.print("\nX : |");
			for(int i = 0; i < X[k].length; i++) {
				System.out.print(" " + X[k][i] + " |");
			}
			//
			// ***************************************************************
			//
			System.out.println("\n\n================================\n");
			for(int i = 0; i < (N-windowSize+1); i++) {
				int j = i + windowSize - 1;
				int i_r = N-i-1;
				
				//String s = "Windows : [" + (i+1) + ", " + (j+1) + "] --> |";
				String s = "Windows : [" + (begin+i+1) + ", " + (begin+j+1) + "] --> |";
				//
				//double contrib = contribs_and_ends[k][1][j] + contribs_and_ends_REV[k][1][i_r]
				//		- contribs_and_ends[k][1][N-1];
				//
				double contrib_ = window_contribution(
						i_r, j, N, contribs_and_ends[k][1], contribs_and_ends_REV[k][1]);
				//
				
				//
				s += " " + contrib_ + " |";
				System.out.println(s);
				
			}
			System.out.println("\n================================\n");
		}
		//System.out.println(
		//		"\n*************************************\n*************************************\n\n");
	}
	/* 
	 * # ---------------------------------------------------------------------------------- #
	 * |                                                                                    |
	 * # ---------------------------------------------------------------------------------- #
	 */
	public static void sliding_windows_contribution_constraint(
			double[][] X, int windowSize, double delta_X, double epsilon, double lambda, 
			byte aggreg, byte feature, byte pattern) {
		//
		int nbMeasures = X.length;
		if(nbMeasures < 1)
			return;
		//
		int N = X[0].length;
		//
		//*
		if(end == 0) {
			begin = 0;
			end = N-1;
		}
		else {
			begin = (end+1)-(windowSize-1);
			end = end + N - (windowSize-1);
		}
		//*/
		//
		double[][] X_r = Functions.reverse_seq(X);
		double[][][] contribs_and_ends = new double[nbMeasures][][]; // contributions and ends
		double[][][] contribs_and_ends_REV = new double[nbMeasures][][]; // reverse contributions and ends
		//
		//System.out.println(
		//"\n\n*************************************\n*************************************\n");
		//
		for(int k=0; k<nbMeasures; k++) {
			//
			//------------------------------
			//
			contribs_and_ends[k] = All_Decorations.contributions_and_ends(
					X[k], delta_X, aggreg, feature, pattern, false);
			contribs_and_ends_REV[k] = All_Decorations.contributions_and_ends(
					X_r[k], delta_X, aggreg, feature, pattern, true);
			//
			//------------------------------
			//
			// ***************************************************************
			//
			//*
//			System.out.println("Measure #" + (k+1) + " : ");
//			System.out.print("\nX : |");
//			for(int i = 0; i < X[k].length; i++) {
//				System.out.print(" " + X[k][i] + " |");
//			}
//			char[] S = Alphabet.getSignature_STR(Alphabet.get_all_Signature(X[k], delta_X));
//			System.out.print("\nS : |");
//			for(int i = 0; i < S.length; i++) {
//				System.out.print("   " + S[i] + "   |"); // | 20.48 |
//			}
			//*/
			//
//			System.out.print("\nX_REV : |");
//			for(int i = 0; i < X[k].length; i++) {
//				System.out.print(" " + X_r[k][i] + " |");
//			}
//			//--------------------------
//			System.out.println("\n\nEnd         : |");
//			for(int i = 0; i < contribs_and_ends[k][0].length; i++) {
//				System.out.print(" " + contribs_and_ends[k][0][i] + " |");
//			}
//			//
//			System.out.println("\nEnd-REV     : |");
//			for(int i = 0; i < contribs_and_ends_REV[k][0].length; i++) {
//				System.out.print(" " + contribs_and_ends_REV[k][0][i] + " |");
//			}
//			//--------------------------
//			System.out.println("\n\nContrib     : |");
//			for(int i = 0; i < contribs_and_ends[k][1].length; i++) {
//				System.out.print(" " + contribs_and_ends[k][1][i] + " |");
//			}
//			//
//			System.out.println("\nContrib-REV : |");
//			for(int i = 0; i < contribs_and_ends_REV[k][1].length; i++) {
//				System.out.print(" " + contribs_and_ends_REV[k][1][i] + " |");
//			}
			//
			// ***************************************************************
			//
			System.out.println("\n================================\n");
			for(int i = 0; i < (N-windowSize+1); i++) {
				int j = i + windowSize - 1;
				int i_r = N-i-1;
				
				//String s = "Windows : [" + (i+1) + ", " + (j+1) + "] --> |";
				String s = "Windows : [" + (begin+i+1) + ", " + (begin+j+1) + "] --> |";
				//
				//double contrib = contribs_and_ends[k][1][j] + contribs_and_ends_REV[k][1][i_r]
				//		- contribs_and_ends[k][1][N-1];
				//
				//*
				double contrib_ = window_contribution(
						i_r, j, N, contribs_and_ends[k][1], contribs_and_ends_REV[k][1]);
				//*/
//				double contrib__ = window_contribution(
//						i_r, j, N, contribs_and_ends[k], contribs_and_ends_REV[k]);
				//s += " " + contrib__ + "(" + contrib_ + ") |";
				s += " " + contrib_ + " |";
				//
				//double percentage = contrib__/windowSize;
				
				double percentage = (((2 * contrib_) + 1) * (1 + lambda)) / windowSize;
				
				s += " --> " + percentage;
				if(percentage >= epsilon) {
					s += " --> ERROR";
					System.out.println(s);
				}
				else {
					s += " --> NO ERROR";
					//System.out.println(s);
				}
				//
				
				
			}
			System.out.println("\n================================\n");
		}
		//System.out.println(
		//"*************************************\n*************************************\n");
	}
	/* 
	 * # ---------------------------------------------------------------------------------- #
	 * |                                                                                    |
	 * # ---------------------------------------------------------------------------------- #
	 */
	public static void sliding_windows_contribution_constraint_BinMultivariate(
			double[][] X, int windowSize, double delta_X, double epsilon, double lambda, 
			byte aggreg, byte feature, byte pattern) {
		//
		int nbMeasures = X.length;
		if(nbMeasures < 1)
			return;
		//
		int N = X[0].length;
		//
		//----------------------------------------------------------------
		//*
		if(end == 0) {
			begin = 0;
			end = N-1;
		}
		else {
			begin = (end+1)-(windowSize-1);
			end = end + N - (windowSize-1);
		}
		//*/
		//----------------------------------------------------------------
		//
		double[][] X_r = Functions.reverse_seq(X);
		//
		double[][] contribs_and_ends = All_Decorations.contributions_and_ends_BinMultivariate(
				X, delta_X, aggreg, feature, pattern, false); // contributions and ends
		double[][] contribs_and_ends_REV = All_Decorations.contributions_and_ends_BinMultivariate(
				X_r, delta_X, aggreg, feature, pattern, true); // reverse contributions and ends
		//
		//----------------------------------------------------------------
		//----------------------------------------------------------------
		//
		System.out.println("\n================================\n");
		for(int i = 0; i < (N-windowSize+1); i++) {
			int j = i + windowSize - 1;
			int i_r = N-i-1;
			
			//String s = "Windows : [" + (i+1) + ", " + (j+1) + "] --> |";
			String s = "Windows : [" + (begin+i+1) + ", " + (begin+j+1) + "] --> |";
			//
			//double contrib = contribs_and_ends[k][1][j] + contribs_and_ends_REV[k][1][i_r]
			//		- contribs_and_ends[k][1][N-1];
			//
			//*
			double contrib_ = window_contribution(
					i_r, j, N, contribs_and_ends[1], contribs_and_ends_REV[1]);
			//*/
//			double contrib__ = window_contribution(
//					i_r, j, N, contribs_and_ends[k], contribs_and_ends_REV[k]);
			//s += " " + contrib__ + "(" + contrib_ + ") |";
			s += " " + contrib_ + " |";
			//
			//double percentage = contrib__/windowSize;
			double percentage = (((2 * contrib_) + 1) * (1 + lambda)) / windowSize;
			//
			s += " --> " + percentage;
			if(percentage >= epsilon) {
				s += " --> ERROR";
				System.out.println(s);
			}
			else {
				s += " --> NO ERROR";
				//System.out.println(s);
			}
		}
		System.out.println("\n================================\n");
		//
	}
	/* 
	 * # ---------------------------------------------------------------------------------- #
	 * |                                                                                    |
	 * # ---------------------------------------------------------------------------------- #
	 */
	public static double[][][][] sliding_windows_contribution_constraint(
			double[][] X, int windowSize, double delta_X, byte aggreg, byte feature, byte pattern) {
		//
		int nbMeasures = X.length;
		//
		double[][] X_r = Functions.reverse_seq(X);
		//
		// 0: contributions and ends, 0: reverse values
		double[][][][] contribs_and_ends = new double[2][nbMeasures][][]; 
		//
		for(int k=0; k<nbMeasures; k++) {
			contribs_and_ends[0][k] = All_Decorations.contributions_and_ends(
					X[k], delta_X, aggreg, feature, pattern, false);
			contribs_and_ends[1][k] = All_Decorations.contributions_and_ends(
					X_r[k], delta_X, aggreg, feature, pattern, true);
		}
		return contribs_and_ends;
	}
	/* 
	 * # ---------------------------------------------------------------------------------- #
	 * |                                                                                    |
	 * # ---------------------------------------------------------------------------------- #
	 */
	public static double[][][] sliding_windows_contribution_constraint_BinMultivariate(
			double[][] X, int windowSize, 
			double delta_X, byte aggreg, byte feature, byte pattern) {
		//
		double[][] X_r = Functions.reverse_seq(X);
		//
		// 0: contributions and ends, 0: reverse values
		double[][][] contribs_and_ends = new double[2][][];
		//
		contribs_and_ends[0] = All_Decorations.contributions_and_ends_BinMultivariate(
				X, delta_X, aggreg, feature, pattern, false);
		contribs_and_ends[1] = All_Decorations.contributions_and_ends_BinMultivariate(
				X_r, delta_X, aggreg, feature, pattern, true);
		//
		return contribs_and_ends;
	}
	/* 
	 * # ---------------------------------------------------------------------------------- #
	 * |                                                                                    |
	 * # ---------------------------------------------------------------------------------- #
	 */
	
}

