/*
    The contents of this file are subject to the Mozilla Public License
    Version  1.1  (the "License"); you may not use this file except in
    compliance with the License. You may obtain a copy of the License at:

    http://www.mozilla.org/MPL/

    Software  distributed  under  the License is distributed on an "AS
    IS"  basis,  WITHOUT  WARRANTY  OF  ANY  KIND,  either  express or
    implied.  See  the  License  for  the  specific language governing
    rights and limitations under the License.
    The Original Code is the contents of this file.
    The Developer of the Original Code is IMT Atlantique
    (4 Rue Alfred Kastler, 44300 Nantes, France).
    Contributor(s):
    _____Arnold Hien <arnold.hien@imt-atlantique.fr>
*/

package fr.imtAtlantique.ls2n.tasc.timeSeries.transducers;

import fr.imtAtlantique.ls2n.tasc.timeSeries.constants.Alphabet;
import fr.imtAtlantique.ls2n.tasc.timeSeries.constants.Semantics;

public class GorgeTransducer {
	
	public static byte[] getTransition(byte currentState, byte letter) {
		// output --> new state and semantic letter
		byte[] output = { -1, -1 }; // output[0] --> new state, output[0] --> semantic letter
		
		switch(currentState) {
			case Transducers.STATE_s:
				if((letter == Alphabet.EQUAL) || (letter == Alphabet.LESSER)) {
					output[0] = currentState;
					output[1] = Semantics.OUT;
				} else {
					output[0] = Transducers.STATE_r;
					output[1] = Semantics.OUT;
				}
				break;
			case Transducers.STATE_r:
				if(letter == Alphabet.GREATER) {
					output[0] = currentState;
					output[1] = Semantics.MAYBE_b;
				} else if(letter == Alphabet.EQUAL) {
					output[0] = Transducers.STATE_u;
					output[1] = Semantics.MAYBE_b;
				} else {
					output[0] = Transducers.STATE_t;
					output[1] = Semantics.FOUND;
				}
				break;
			case Transducers.STATE_t:
				if(letter == Alphabet.EQUAL) {
					output[0] = currentState;
					output[1] = Semantics.MAYBE_a;
				} else if(letter == Alphabet.LESSER) {
					output[0] = currentState;
					output[1] = Semantics.IN;
				} else {
					output[0] = Transducers.STATE_r;
					output[1] = Semantics.OUT_a;
				}
				break;
			case Transducers.STATE_u:
				if(letter == Alphabet.EQUAL) {
					output[0] = currentState;
					output[1] = Semantics.MAYBE_b;
				} else if(letter == Alphabet.LESSER) {
					output[0] = Transducers.STATE_s;
					output[1] = Semantics.OUT_r;
				} else {
					output[0] = Transducers.STATE_r;
					output[1] = Semantics.MAYBE_b;
				}
				break;
		}
		return output;
	}
	
	/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	
	public static byte[] getReverseTransition(byte currentState, byte letter) {
		return getTransition(currentState, letter);
	}
	
	/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	
	public static byte[] getReverseTransition_(byte currentState, byte letter) {
		// output --> new state and semantic letter
		byte[] output = { -1, -1 }; // output[0] --> new state, output[0] --> semantic letter
		
		switch(currentState) {
			case Transducers.STATE_s:
				if((letter == Alphabet.EQUAL) || (letter == Alphabet.GREATER)) {
					output[0] = currentState;
					output[1] = Semantics.OUT;
				} else {
					output[0] = Transducers.STATE_r;
					output[1] = Semantics.OUT;
				}
				break;
			case Transducers.STATE_r:
				if(letter == Alphabet.LESSER) {
					output[0] = currentState;
					output[1] = Semantics.MAYBE_b;
				} else if(letter == Alphabet.EQUAL) {
					output[0] = Transducers.STATE_u;
					output[1] = Semantics.MAYBE_b;
				} else {
					output[0] = Transducers.STATE_t;
					output[1] = Semantics.FOUND;
				}
				break;
			case Transducers.STATE_t:
				if(letter == Alphabet.EQUAL) {
					output[0] = currentState;
					output[1] = Semantics.MAYBE_a;
				} else if(letter == Alphabet.GREATER) {
					output[0] = currentState;
					output[1] = Semantics.IN;
				} else {
					output[0] = Transducers.STATE_r;
					output[1] = Semantics.OUT_a;
				}
				break;
			case Transducers.STATE_u:
				if(letter == Alphabet.EQUAL) {
					output[0] = currentState;
					output[1] = Semantics.MAYBE_b;
				} else if(letter == Alphabet.GREATER) {
					output[0] = Transducers.STATE_s;
					output[1] = Semantics.OUT_r;
				} else {
					output[0] = Transducers.STATE_r;
					output[1] = Semantics.MAYBE_b;
				}
				break;
		}
		return output;
	}
	
}
