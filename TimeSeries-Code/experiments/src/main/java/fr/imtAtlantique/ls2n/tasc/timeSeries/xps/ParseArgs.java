/*
    The contents of this file are subject to the Mozilla Public License
    Version  1.1  (the "License"); you may not use this file except in
    compliance with the License. You may obtain a copy of the License at:

    http://www.mozilla.org/MPL/

    Software  distributed  under  the License is distributed on an "AS
    IS"  basis,  WITHOUT  WARRANTY  OF  ANY  KIND,  either  express or
    implied.  See  the  License  for  the  specific language governing
    rights and limitations under the License.
    The Original Code is the contents of this file.
    The Developer of the Original Code is IMT Atlantique
    (4 Rue Alfred Kastler, 44300 Nantes, France).
    Contributor(s):
    _____Arnold Hien <arnold.hien@imt-atlantique.fr>
*/

package fr.imtAtlantique.ls2n.tasc.timeSeries.xps;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import fr.imtAtlantique.ls2n.tasc.timeSeries.constants.Constants;
import fr.imtAtlantique.ls2n.tasc.timeSeries.functions.aggreg.Aggregator;
import fr.imtAtlantique.ls2n.tasc.timeSeries.functions.features.Features;

public class ParseArgs {

	protected static Options get_options() {
		//
		Options all_options = new Options();
		// ------------------------------------------------------------
		Option pattern = new Option("p", "pattern", true, "Pattern - default : PEAK --> REQUIRED \n\n");
		pattern.setRequired(true);
		all_options.addOption(pattern);
		// ------------------------------------------------------------
		Option feature = new Option("f", "feature", true, "Feature --> REQUIRED \n\n");
		feature.setRequired(true);
		all_options.addOption(feature);
		// ------------------------------------------------------------
		Option aggreg = new Option("a", "aggreg", true, "Aggregation --> REQUIRED \n\n");
		aggreg.setRequired(true);
		all_options.addOption(aggreg);
		// ------------------------------------------------------------
		Option window = new Option("w", "window", true, "Window length --> REQUIRED \n\n");
		window.setRequired(true);
		all_options.addOption(window);
		// ------------------------------------------------------------
		Option deltaX = new Option("dx", "deltaX", true,
				"Min value between beyond which two numbers are considered differents - default : 0.01 \n\n");
		deltaX.setRequired(true);
		all_options.addOption(deltaX);
		// ------------------------------------------------------------
		Option wPercent = new Option("wp", "wPercent", true, "Occupancy percentage of a window  - default : 0.25 \n\n");
		wPercent.setRequired(true);
		all_options.addOption(wPercent);
		// ------------------------------------------------------------
		Option overlap = new Option("ov", "overlap", true, "Used to evaluate the occupancy percentage \n\n");
		overlap.setRequired(false);
		all_options.addOption(overlap);
		// ------------------------------------------------------------
		Option inputData = new Option("d", "data", true, "Input data \n\n");
		inputData.setRequired(true);
		all_options.addOption(inputData);
		//
		Option help = new Option("h", "help", false, "help \n");
		help.setRequired(false);
		all_options.addOption(help);
		//
		return all_options;
	}

	//
	// *******************************************************************************************
	// *******************************************************************************************
	//
	protected static CommandLine get_parser(String[] args) {
		//
		Options all_options = get_options();
		//
		CommandLine cmd = null;
		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		//
		String help_msg = "\n\n\t\t***** HELP *****\n\tlist of arguments\n\n";
		String error_msg = "\nError : missing arguments";
		//
		try {
			cmd = parser.parse(all_options, args);
			if (cmd.hasOption("h")) {
				formatter.printHelp("utility-name", all_options);
				System.exit(0);
			} else if (!cmd.hasOption("p") || !cmd.hasOption("f") || !cmd.hasOption("a") || !cmd.hasOption("w")
					|| !cmd.hasOption("dx") || !cmd.hasOption("wp") || !cmd.hasOption("ov") || !cmd.hasOption("d")) {
				formatter.printHelp(help_msg, all_options);
				System.exit(0);
			}
		} catch (ParseException e) {
			System.out.println(e);
			e.printStackTrace();
			System.exit(1);
		} catch (Throwable e) {
			System.out.println("Undefined error");
			formatter.printHelp(error_msg, all_options);
			e.printStackTrace();
			System.exit(1);
		}
		//
		return cmd;
	}
	//
	// *******************************************************************************************
	// *******************************************************************************************
	//
	protected static byte getPattern(String pattern_STR) {
		//
		byte pattern;
		switch (pattern_STR.toLowerCase()) {
			case "inflexion":
				pattern = Constants.Inflexion;
				break;
				//--------------------------------------------------
			case "bumpondecseq":
				pattern = Constants.BumpOnDecSeq;
				break;
			case "diponincseq":
				pattern = Constants.DipOnIncSeq;
				break;
				//--------------------------------------------------
			case "dec":
				pattern = Constants.Dec;
				break;
			case "inc":
				pattern = Constants.Inc;
				break;
			case "steady":
				pattern = Constants.Steady;
				break;
				//--------------------------------------------------
			case "decterrace":
				pattern = Constants.DecTerrace;
				break;
			case "incterrace":
				pattern = Constants.IncTerrace;
				break;
				//--------------------------------------------------
			case "plain":
				pattern = Constants.Plain;
				break;
			case "plateau":
				pattern = Constants.Plateau;
				break;
			case "properplain":
				pattern = Constants.ProperPlain;
				break;
			case "properplateau":
				pattern = Constants.ProperPlateau;
				break;
				//--------------------------------------------------
			case "gorge":
				pattern = Constants.Gorge;
				break;
			case "summit":
				pattern = Constants.Summit;
				break;
			case "peak":
				pattern = Constants.Peak;
				break;
			case "valley":
				pattern = Constants.Valley;
				break;
				//--------------------------------------------------
			case "decseq":
				pattern = Constants.DecSeq;
				break;
			case "incseq":
				pattern = Constants.IncSeq;
				break;
			case "steadyseq":
				pattern = Constants.SteadySeq;
				break;
			case "strictlydecseq":
				pattern = Constants.StrictlyDecSeq;
				break;
			case "strictlyincseq":
				pattern = Constants.StrictlyIncSeq;
				break;
				//--------------------------------------------------
			case "zigzag":
				pattern = Constants.Zigzag;
				break;
				//--------------------------------------------------
			case "temphumiddec":
				pattern = Constants.TempHumidDec;
				break;
			case "temphumidinc":
				pattern = Constants.TempHumidInc;
				break;
				//--------------------------------------------------
			default:
				pattern = Constants.Peak;
				break;
				//--------------------------------------------------
		}
		//
		return pattern;
	}
	//
	// *******************************************************************************************
	// *******************************************************************************************
	//
	protected static byte getFeature(String feature_STR) {
		//
		byte feature;
		switch (feature_STR.toLowerCase()) {
			case "one":
				feature = Features.One_f;
				break;
				//--------------------------------------------------
			case "width":
				feature = Features.Width_f;
				break;
				//--------------------------------------------------
			case "surface":
				feature = Features.Surface_f;
				break;
				//--------------------------------------------------
			case "max":
				feature = Features.Max_f;
				break;
				//--------------------------------------------------
			case "min":
				feature = Features.Min_f;
				break;
				//--------------------------------------------------
			default:
				feature = Features.One_f;
				break;
				//--------------------------------------------------
		}
		//
		return feature;
	}
	//
	// *******************************************************************************************
	// *******************************************************************************************
	//
	protected static byte getAggreg(String aggreg_STR) {
		//
		byte aggreg;
		switch (aggreg_STR.toLowerCase()) {
			case "sum":
				aggreg = Aggregator.Sum_g;
				break;
				//--------------------------------------------------
			case "min":
				aggreg = Aggregator.Min_g;
				break;
				//--------------------------------------------------
			case "max":
				aggreg = Aggregator.Max_g;
				break;
				//--------------------------------------------------
			default:
				aggreg = Aggregator.Sum_g;
				break;
				//--------------------------------------------------
		}
		//
		return aggreg;
	}
	//
	// *******************************************************************************************
	// *******************************************************************************************
	//
	
}
