/*
    The contents of this file are subject to the Mozilla Public License
    Version  1.1  (the "License"); you may not use this file except in
    compliance with the License. You may obtain a copy of the License at:

    http://www.mozilla.org/MPL/

    Software  distributed  under  the License is distributed on an "AS
    IS"  basis,  WITHOUT  WARRANTY  OF  ANY  KIND,  either  express or
    implied.  See  the  License  for  the  specific language governing
    rights and limitations under the License.
    The Original Code is the contents of this file.
    The Developer of the Original Code is IMT Atlantique
    (4 Rue Alfred Kastler, 44300 Nantes, France).
    Contributor(s):
    _____Arnold Hien <arnold.hien@imt-atlantique.fr>
*/

package fr.imtAtlantique.ls2n.tasc.timeSeries.xps.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DataGenerator {
	//
	public static int generateDataInt(int inf, int sup) {
		Random rand = new Random();
		int val = rand.nextInt(inf, sup);
		assert val >= inf: "Value generate by DataGenerator is not correct (value < minimum val)";
		return val;
	}
	//
	public static float generateDataFloat(int inf, int sup) {
		Random rand = new Random();
		float val = rand.nextFloat(inf, (float) (sup));
		assert val >= inf: "Value generate by DataGenerator is not correct (value < minimum val)";
		return val;
	}
	//
	public static double generateDataFloat(double inf, double sup) {
		Random rand = new Random();
		double val = rand.nextDouble(inf, (float) (sup));
		assert val >= inf: "Value generate by DataGenerator is not correct (value < minimum val)";
		return val;
	}
	//
	public static List<Integer> generateDataInt(int n) {
		Random rand = new Random();
		List<Integer> list = new ArrayList<Integer>();
		for(int i = 0; i < n; i++) {
			int val = rand.nextInt();
			assert val >= 0: "Value generate by DataGenerator is not correct (value < minimum val)";
			list.add(val);
		}
		return list;
	}
	//
	public static List<Integer> generateDataInt(int n, int inf, int sup) {
		Random rand = new Random();
		List<Integer> list = new ArrayList<Integer>();
		for(int i = 0; i < n; i++) {
			int val = rand.nextInt(sup) + inf;
			assert val >= inf: "Value generate by DataGenerator is not correct (value < minimum val)";
			list.add(val);
		}
		return list;
	}
	//
	public static int[] generateDataTabInt(int n) {
		Random rand = new Random();
		int[] tab = new int[n];
		for(int i = 0; i < n; i++) {
			int val = rand.nextInt();
			assert val >= 0: "Value generate by DataGenerator is not correct (value < minimum val)";
			tab[i] = val;
		}
		return tab;
	}
	//
	public static int[]  generateDataTabInt(int n, int inf, int sup) {
		Random rand = new Random();
		int[] tab = new int[n];
		for(int i = 0; i < n; i++) {
			int val = rand.nextInt(sup) + inf;
			assert val >= inf: "Value generate by DataGenerator is not correct (value < minimum val)";
			tab[i] = val;
		}
		
		return tab;
	}
	//
	public static List<Float> generateDataFloat(int n) {
		Random rand = new Random();
		List<Float> list = new ArrayList<Float>();
		for(int i = 0; i < n; i++) {
			float val = rand.nextFloat();
			assert val >= 0: "Value generate by DataGenerator is not correct (value < minimum val)";
			list.add(val);
		}
		return list;
	}
	//
	public static List<Float> generateDataFloat(int n, float inf, float sup) {
		Random rand = new Random();
		List<Float> list = new ArrayList<Float>();
		for(int i = 0; i < n; i++) {
			float val = rand.nextFloat(sup) + inf;
			assert val >= inf: "Value generate by DataGenerator is not correct (value < minimum val)";
			list.add(val);
		}
		return list;
	}
	//
	public static float generateDataFloat(float inf, float sup) {
		Random rand = new Random();
		float val = rand.nextFloat(sup) + inf;
		assert val >= inf: "Value generate by DataGenerator is not correct (value < minimum val)";
		return val;
	}
	//
	public static float[] generateDataTabFloat(int n) {
		Random rand = new Random();
		float[] tab = new float[n];
		for(int i = 0; i < n; i++) {
			float val = rand.nextInt();
			assert val >= 0: "Value generate by DataGenerator is not correct (value < minimum val)";
			tab[i] = val;
		}
		return tab;
	}
	//
	public static float[]  generateDataTabFloat(int n, float inf, float sup) {
		Random rand = new Random();
		float[] tab = new float[n];
		for(int i = 0; i < n; i++) {
			float val = rand.nextFloat(sup) + inf;
			assert val >= inf: "Value generate by DataGenerator is not correct (value < minimum val)";
			tab[i] = val;
		}
		return tab;
	}
	
}
