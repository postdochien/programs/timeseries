/*
    The contents of this file are subject to the Mozilla Public License
    Version  1.1  (the "License"); you may not use this file except in
    compliance with the License. You may obtain a copy of the License at:

    http://www.mozilla.org/MPL/

    Software  distributed  under  the License is distributed on an "AS
    IS"  basis,  WITHOUT  WARRANTY  OF  ANY  KIND,  either  express or
    implied.  See  the  License  for  the  specific language governing
    rights and limitations under the License.
    The Original Code is the contents of this file.
    The Developer of the Original Code is IMT Atlantique
    (4 Rue Alfred Kastler, 44300 Nantes, France).
    Contributor(s):
    _____Arnold Hien <arnold.hien@imt-atlantique.fr>
*/

package fr.imtAtlantique.ls2n.tasc.timeSeries.xps;

import org.apache.commons.cli.CommandLine;

import fr.imtAtlantique.ls2n.tasc.timeSeries.xps.computing.Computer;
import fr.imtAtlantique.ls2n.tasc.timeSeries.xps.data.DataHandler;

public class Main {
	private static byte pattern = 15; // Peak
	private static byte feature = 3;
	private static byte aggreg = 3;
	private static int window = 10;
	private static double deltaX = 0.01;
	private static double wPercent = 0.25;
	private static double overlap = 0.1;
	private static String inputData = "";
	//
	//*******************************************************************************************
	//*******************************************************************************************
	//
	public static void parse_args(String[] args) {
		CommandLine parse_cmd = ParseArgs.get_parser(args);
		//
		if (parse_cmd == null) {
			// There's no parameter set
			System.exit(0);
		} else {
			pattern = ParseArgs.getPattern(parse_cmd.getOptionValue("p"));
			feature = ParseArgs.getFeature(parse_cmd.getOptionValue("f"));
			aggreg = ParseArgs.getAggreg(parse_cmd.getOptionValue("a"));
			//
			window = Integer.parseInt(parse_cmd.getOptionValue("w"));
			deltaX = Double.parseDouble(parse_cmd.getOptionValue("dx"));
			wPercent = Double.parseDouble(parse_cmd.getOptionValue("wp"));
			overlap = Double.parseDouble(parse_cmd.getOptionValue("ov"));
			//
			inputData = parse_cmd.getOptionValue("d");
			//
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//
		parse_args(args);
		double [][] streamData = null;
		//streamData = DataHandler.readCSVData(inputData);
		//streamData = DataHandler.generateData(1, 100, 1, 5);
		streamData = DataHandler.readCSVData(inputData, (pattern >= 23));
		Computer.compute_anomaly_detection(streamData, window, deltaX, wPercent, overlap, pattern, feature, aggreg);
		//
	}

}
