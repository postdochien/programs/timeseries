/*
    The contents of this file are subject to the Mozilla Public License
    Version  1.1  (the "License"); you may not use this file except in
    compliance with the License. You may obtain a copy of the License at:

    http://www.mozilla.org/MPL/

    Software  distributed  under  the License is distributed on an "AS
    IS"  basis,  WITHOUT  WARRANTY  OF  ANY  KIND,  either  express or
    implied.  See  the  License  for  the  specific language governing
    rights and limitations under the License.
    The Original Code is the contents of this file.
    The Developer of the Original Code is IMT Atlantique
    (4 Rue Alfred Kastler, 44300 Nantes, France).
    Contributor(s):
    _____Arnold Hien <arnold.hien@imt-atlantique.fr>
*/

package fr.imtAtlantique.ls2n.tasc.timeSeries.xps.data;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;

public class DataHandler {
	
	//
	//-----------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------
	//
	public static double[][] generateData(int nbMeasures, int streamLength, double minVal, double maxVal) {
		double[][] allDataToSend = new double[nbMeasures][streamLength];
		for(int i = 0; i < nbMeasures; i++) {
			for(int j = 0; j < streamLength; j++) {
				if(j < streamLength-1) {
					allDataToSend[i][j] = DataGenerator.generateDataFloat(minVal, maxVal);
				}
				else {
					allDataToSend[i][j] = DataGenerator.generateDataFloat(minVal, maxVal);
				}
			}
		}
		return allDataToSend;
	}
	//
	//-----------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------
	//
	public static double[][] readCSVData(String inputDataFile, boolean bin) {
		if(bin)
			return readCSVData_BinMultivariate(inputDataFile);
		else
			return readCSVData_Univariate(inputDataFile);
	}
	//
	//-----------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------
	//
	public static double[][] readCSVData_Univariate(String inputDataFile) {
		FileReader filereader;
		double[][] allDataToSend = null;
		try {
			filereader = new FileReader(inputDataFile); // Create file reader with CSV file as a parameter.
			//
			//****************************************************************
			//
			List<String> allData = new ArrayList<String>();
			try (CSVReader csvReader = new CSVReader(filereader)) {
				String[] nextLine;
				csvReader.readNext(); // header
				while ( (nextLine = csvReader.readNext()) != null ) {
					allData.add(nextLine[1]);
				}
			}
			int csv_size = allData.size(); //250000; // allData.size()
			allDataToSend = new double[1][csv_size];
			for(int i=0; i<csv_size; i++) {
				allDataToSend[0][i] = Double.parseDouble(allData.get(i));
			}
			//
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException | CsvException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return allDataToSend;
	}
	//
	//-----------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------
	//
	public static double[][] readCSVData_BinMultivariate(String inputDataFile) {
		FileReader filereader;
		double[][] allDataToSend = null;
		try {
			filereader = new FileReader(inputDataFile); // Create file reader with CSV file as a parameter.
			//
			//****************************************************************
			//
			List<String> allData = new ArrayList<String>();
			try (CSVReader csvReader = new CSVReader(filereader)) {
				String[] nextLine;
				csvReader.readNext(); // header
				while ( (nextLine = csvReader.readNext()) != null ) {
					if(nextLine[0].equals("error") || nextLine[0].equals("error") || 
							nextLine[0].equals("") || nextLine[0].equals(""))
						continue;
					allData.add(nextLine[1] + "@" + nextLine[2]);
					//System.out.println(nextLine[1] + "@" + nextLine[2]);
				}
			}
			int csv_size = allData.size(); //250000; // allData.size()
			allDataToSend = new double[2][csv_size];
			for(int i=0; i<csv_size; i++) {
				String[] elt = allData.get(i).split("@");
				allDataToSend[0][i] = Double.parseDouble(elt[0]);
				allDataToSend[1][i] = Double.parseDouble(elt[1]);
			}
			//
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException | CsvException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return allDataToSend;
	}

}
