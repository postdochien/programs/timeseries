/*
    The contents of this file are subject to the Mozilla Public License
    Version  1.1  (the "License"); you may not use this file except in
    compliance with the License. You may obtain a copy of the License at:

    http://www.mozilla.org/MPL/

    Software  distributed  under  the License is distributed on an "AS
    IS"  basis,  WITHOUT  WARRANTY  OF  ANY  KIND,  either  express or
    implied.  See  the  License  for  the  specific language governing
    rights and limitations under the License.
    The Original Code is the contents of this file.
    The Developer of the Original Code is IMT Atlantique
    (4 Rue Alfred Kastler, 44300 Nantes, France).
    Contributor(s):
    _____Arnold Hien <arnold.hien@imt-atlantique.fr>
*/

package fr.imtAtlantique.ls2n.tasc.timeSeries.xps.computing;

import fr.imtAtlantique.ls2n.tasc.timeSeries.constants.Constants;
import fr.imtAtlantique.ls2n.tasc.timeSeries.constants.Alphabet;
import fr.imtAtlantique.ls2n.tasc.timeSeries.functions.Functions;

public class Computer {
	
	public static void compute_anomaly_detection(
			double[][] streamData, int window, double deltaX, double epsilon, double lambda,
			byte pattern, byte feature, byte aggreg) {
		//
		if(streamData == null) {
			System.out.println("ERROR: DATA missing");
			System.exit(1);
		}
		//
		if((pattern == Constants.TempHumidInc) || (pattern == Constants.TempHumidDec)) {
			compute_anomaly_detection_BinMultivariate(
					streamData, window, deltaX, epsilon, lambda, pattern, feature, aggreg);
		}
		else {
			compute_anomaly_detection_Univariate(
					streamData, window, deltaX, epsilon, lambda, pattern, feature, aggreg);
		}
		
	}
	/* 
	 * # ---------------------------------------------------------------------------------- #
	 * |                                                                                    |
	 * # ---------------------------------------------------------------------------------- #
	 */
	public static void compute_anomaly_detection_Univariate(
			double[][] streamData, int window, double deltaX, double epsilon, double lambda,
			byte pattern, byte feature, byte aggreg) {
		//
		
		double[][][][] contributions_and_ends = Functions.sliding_windows_contribution_constraint(
				streamData, window, deltaX, aggreg, feature, pattern); 
		//
		//System.out.println("======================================");
		System.out.println("Windows;Aggreg-Feature-Pattern;Percentage");
		for(int k=0; k<streamData.length; k++) {
			int N = streamData[k].length;
			for(int i = 0; i < (N-window+1); i++) {
				int j = i + window - 1;
				int i_r = N-i-1;
				//
				//double contrib = contribs_and_ends[k][1][j] + contribs_and_ends_REV[k][1][i_r]
				//		- contribs_and_ends[k][1][N-1];
				//
				double contrib_ = Functions.window_contribution(
						i_r, j, N, contributions_and_ends[0][k][1], contributions_and_ends[1][k][1]);
				//
				double percentage = (((2 * contrib_) + 1) * (1 + lambda)) / window;
				//
				String s = "" + (i+1) + "-" + (j+1) + " ; " + contrib_ + " ; " + percentage;
				/*
				if(percentage >= epsilon) {
					//s += " --> ERROR";
					System.out.println(s);
				}
				*/
				System.out.println(s);
			}
		}
		//
	}
	/* 
	 * # ---------------------------------------------------------------------------------- #
	 * |                                                                                    |
	 * # ---------------------------------------------------------------------------------- #
	 */
	public static void compute_anomaly_detection_BinMultivariate(
			double[][] streamData, int window, double deltaX, double epsilon, double lambda,
			byte pattern, byte feature, byte aggreg) {
		//
		if(streamData.length < 2)
			return;
		//
		double[][][] contributions_and_ends = 
				Functions.sliding_windows_contribution_constraint_BinMultivariate(
						streamData, window, deltaX, aggreg, feature, pattern);
		//
		//******************************************
		//
		/*
		int deb = 27, fin = 58;
		System.out.println("\n\n--------------------------------\n--------------------------------");
		for(int i = deb; i < fin; i++) {
			System.out.print(" | " + streamData[0][i]);
		}
		System.out.println(" |\n");
		for(int i = deb; i < fin; i++) {
			System.out.print(" | " + streamData[1][i]);
		}
		System.out.println(" |\n--------------------------------");
		for(int i = deb; i < fin; i++) {
			byte sign = Alphabet.getBinMultivariateSignature(streamData, i, i+1, deltaX);
			System.out.print(" | " + Alphabet.getSignature_STR_(sign));
		}
		System.out.println(" |\n--------------------------------");
		System.out.println(contributions_and_ends[0][0].length);
		System.out.println("--------------------------------");
		for(int i = deb; i < fin; i++) {
			System.out.print(" | " + contributions_and_ends[0][0][i] );
		}
		System.out.println(" |\n\n--------------------------------\n--------------------------------\n\n");
		*/
		//
		//******************************************
		//
		int N = streamData[0].length;
		//System.out.println("Size : " + N);
		System.out.println("Windows;Aggreg-Feature-Pattern;Percentage");
		for(int i = 0; i < (N-window+1); i++) {
			int j = i + window - 1;
			int i_r = N-i-1;
			//
			//double contrib = contribs_and_ends[1][j] + contribs_and_ends_REV[1][i_r]
			//		- contribs_and_ends[1][N-1];
			//
			double contrib_ = Functions.window_contribution(
					i_r, j, N, contributions_and_ends[0][1], contributions_and_ends[1][1]);
			//
			double percentage = ((2 * contrib_) * (1 + lambda)) / window;
			percentage = contrib_ / window;
			//
			String s = "" + (i+1) + "-" + (j+1) + " ; " + contrib_ + " ; " + percentage;
			/*
			if(percentage >= epsilon) {
				//s += " --> ERROR";
				System.out.println(s);
			}
			*/
			System.out.println(s);
		}
	}
	//
}
